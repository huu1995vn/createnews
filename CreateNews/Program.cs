﻿using Commons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreateNews
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (StreamReader r = new StreamReader("../../Data/data.json"))
            {

                var json = r.ReadToEnd();
                var items = JsonConvert.DeserializeObject<JObject>(json);
                var environment = CommonMethods.ConvertToString(items["environment"], "settings_dev");
                var settings = JObject.FromObject(items[environment]);
                Variables.settings = settings;
                Variables.data_master = JObject.FromObject(items["master"]);
                Variables.domainApiToken = CommonMethods.ConvertToString(settings["domainApiToken"], "http://192.168.11.3");
                Variables.domainApi = CommonMethods.ConvertToString(settings["domainApi"], "http://192.168.11.3");
                Variables.domainApiCdn = CommonMethods.ConvertToString(settings["domainApiCdn"], "http://192.168.11.3");
                Variables.folderId = CommonMethods.ConvertToInt32(settings["folderId"], 1394);
                Variables.token = CommonMethods.ConvertToString(settings["token"]);
                Variables.userNameAdmin = CommonMethods.ConvertToString(settings["userNameAdmin"], "tronghuu");
                Variables.passAdmin = CommonMethods.ConvertToString(settings["passAdmin"], "40AC3FAC18223DAF66403777BB902108");
                Variables.refresh();
            }   
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLogin());

            
        }
    }
}
