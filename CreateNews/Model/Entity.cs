﻿using Commons;
using CreateNews;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TinTuc
    {
        public int Id { get; set; }
        public int IdFileDaiDien { get; set; }
        public string MoTaNgan { get; set; }
        public string Content { get; set; }
        public string RewriteUrl { get; set; }
        public string UrlHinhDaiDien { get; set; }
        public int TrangThai { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgayCapNhat { get; set; }
        public int IdMenu { get; set; }// 2,
        public int IdLoaiTinTuc { get; set; }// 1,
        public string TieuDeTinTuc { get; set; }// "Quảng bá rầm rộ nhưng thị phần xe điện ở Mỹ chỉ đạt mức 1%",
        public string UrlPrefix { get; set; }// "tin-tuc",
        public int GetNewsId { get; set; }// 9,
        public string HashTagsInChiTiet { get; set; }// "",
        public DateTime NgayUp { get; set; }// "2019/09/24 16:07:38",
        public DateTime NgayUpFacebook { get; set; }// "2019/09/24 16:07:38",
        public DateTime NgayUpZalo { get; set; }// "2019-09-24T09:07:38.415Z",
        public string ComponentDataTinTucList { get; set; }// "49",
        public string LinkGoc { get; set; }
        
        public int IdSeo { get; set; }// 0,
        
        public TinTuc()
        {
            Id = -1;
            IdMenu = 2;
            IdLoaiTinTuc = 1;
            GetNewsId = 9;
            UrlPrefix = "tin-tuc";
            TrangThai = 1;
            UrlHinhDaiDien = "";
            //NgayTao = new DateTime();
            //NgayUp = new DateTime();

        }

        public JObject ToObject()
        {
            JObject body = JObject.FromObject(this);
            body.Remove("Id");
            body.Remove("NgayUpFacebook");
            body.Remove("NgayUpZalo");
            body["NgayTao"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            body["NgayCapNhat"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            return body;
        }
    }

    public class ImageFile { 
        public string TenAdminTao { get; set; }
        public string TenAdminCapNhat { get; set; }
        public string TenAdminXoa { get; set; }
        public int CapDo { get; set; }
        public string DefaultImageSrc { get; set; }
        public string UrlHinhAnh { get; set; }
        public string UrlFile { get; set; }
        public string Folder { get; set; }
        public int RowIndex { get; set; }
        public int DisplayPage { get; set; }
        public int IdFoldersParentList { get; set; }
        public string DuplicateName { get; set; }
        public int Id { get; set; }
        public int FolderId { get; set; }
        public string Name { get; set; }
        public int FileSize { get; set; }
        public int DuplicateNumber { get; set; }
        public string ImageUrl { get; set; }
        public string Extension { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int IdAdminTao { get; set; }
        public DateTime NgayTao { get; set; }
        public int IdAdminCapNhat { get; set; }
        public DateTime NgayCapNhat { get; set; }
        public string FullName { get; set; }
        public int IdAdminXoa { get; set; }
        public DateTime NgayXoa { get; set; }
        public int TrangThai { get; set; }
        public string GhiChu { get; set; }
        public string TieuDe { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public string SystemName { get; set; }
        public ImageFile()
        {
            this.Id = -1;
            this.FolderId = Variables.folderId;
        }
    }

    public class CustomResultType<T>
    {
        private int _intResult = 1;      // -1: Het session, 0: Không có dữ liệu, 1: OK, -2: Lỗi
        private string _strResult = string.Empty;
        private IEnumerable<T> _dataResult;
        private string _message = string.Empty;
        private string _gotoLink = string.Empty;
        private string _httpResponseCode = "";
        private bool _cacheable = false;
        private PaginationHeader _pagination = new PaginationHeader();

        public CustomResultType()
        {

        }

        //public async Task Execute(string requestUri, string httpType, object bodyData, string defaultfolderId, params HttpParam[] arrParams)
        //{
        //    string res = "";
        //    try
        //    {
        //        ApiGetCore<T> core = new ApiGetCore<T>();
        //        res = await core.GetAsyncReturnString(requestUri, httpType, bodyData, defaultfolderId, arrParams);

        //        if (!string.IsNullOrEmpty(res))
        //        {
        //            // nguyencuongcs 20181230: nhận diện api trả về bị lỗi -> ko cập nhật cache data, sẽ bổ sung sau
        //            // if (Enum.IsDefined(typeof(HttpStatusCode), res))
        //            // {
        //            //     SetExceptionHttpError(res);
        //            // }
        //            // else
        //            // {
        //            JObject resObj;
        //            try
        //            {
        //                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(res);
        //                resObj = JObject.Parse(obj.ToString());
        //            }
        //            catch
        //            {
        //                resObj = JObject.Parse(res);
        //            }
        //            LoadProperty(resObj);
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string error = ex.ToString();

        //        // string error = ex.ToString();
        //        if (res.ToLower() == CommonMethods.ConvertToString(HttpStatusCode.BadRequest).ToLower())
        //        {
        //            Message = Messages.ERR_Http_BadRequest;
        //        }
        //        else if (res.ToLower() == CommonMethods.ConvertToString(HttpStatusCode.Unauthorized).ToLower())
        //        {
        //            Message = Messages.ERR_Http_UnAuthorized;
        //        }
        //        else
        //        {
        //            Message = Messages.ERR_004;
        //        }
        //    }
        //}

        public CustomResultType(JObject obj)
        {
            LoadProperty(obj);
        }

        private void LoadProperty(JObject obj)
        {
            // nguyencuongcs 20181230
            HttpResponseCode = "200";
            if (obj != null)
            {
                var rootJfolderId = obj.Root;
                //CommonMethods.DeserializeObject<IEnumerable<CauHinhHeThong>>(obj.Root["DataResult"]);
                if (rootJfolderId != null)
                {
                    // Vì property Set của Message gán lại _intResult nên phải lấy trước
                    Message = rootJfolderId["Message"] != null ? rootJfolderId["Message"].ToString() : string.Empty;
                    StrResult = rootJfolderId["StrResult"] != null ? rootJfolderId["StrResult"].ToString() : string.Empty;
                    DataResult = CommonMethods.DeserializeObject<List<T>>(rootJfolderId["DataResult"]);
                    Pagination = CommonMethods.DeserializeObject<PaginationHeader>(rootJfolderId["Pagination"]);
                    GotoLink = rootJfolderId["GotoLink"] != null ? rootJfolderId["GotoLink"].ToString() : string.Empty;

                    IntResult = CommonMethods.DeserializeObject<int>(rootJfolderId["IntResult"]);
                    Cacheable = true;
                }
            }
        }

        public int IntResult
        {
            get { return this._intResult; }
            set { this._intResult = value; }
        }
        public string StrResult
        {
            get { return this._strResult; }
            set { this._strResult = value; }
        }
        public IEnumerable<T> DataResult
        {
            get { return this._dataResult; }
            set
            {
                this._dataResult = value;
                this._intResult = this._dataResult != null ? 1 : 0;
            }
        }
        public PaginationHeader Pagination
        {
            get { return this._pagination; }
            set { this._pagination = value; }
        }
        public string Message
        {
            get { return this._message; }
            set
            {
                this._message = value;
                this._intResult = 0;
            }
        }
        public string GotoLink
        {
            get { return this._gotoLink; }
            set { this._gotoLink = value; }
        }
        public string HttpResponseCode
        {
            get { return this._httpResponseCode; }
            set { this._httpResponseCode = value; }
        }
        public bool Cacheable
        {
            get { return this._cacheable; }
            set { this._cacheable = value; }
        }
        public void SetMessageLogout()
        {
            this._intResult = -1;
            this._message = Variables.MessageSessionTimeOut;
        }
        public void SetMessageInvalidRole()
        {
            this._intResult = -1;
            this._message = Variables.MessageSessionInvalidRole;
        }
        public void SetException(Exception ex)
        {
            this._intResult = -2;
            this._message = ex.Message;
        }
        public void SetException(string msg)
        {
            this._intResult = -2;
            this._message = msg;
        }
        public void SetExceptionInvalidAccount()
        {
            this._intResult = -3;
            this._message = Messages.ERR_Http_UnAuthorized;
        }
        public void SetExceptionHttpError(string errorCode)
        {
            this._intResult = -4;
            this._httpResponseCode = errorCode;
            this._cacheable = false;
            this._message = string.Format(Messages.ERR_Http_Error, errorCode);
        }
        public void AddPagination(int currentPage, int displayItems, int resultCount, long totalItems)
        {
            var totalPages = (long)Math.Round((double)totalItems / displayItems, 0, MidpointRounding.AwayFromZero);

            this.Pagination.CurrentPage = currentPage;
            this.Pagination.DisplayItems = displayItems;
            this.Pagination.ResultCount = resultCount;
            this.Pagination.TotalItems = totalItems;

            if (totalPages < 1) { totalPages = 1; }

            this.Pagination.TotalPages = totalPages;
        }
    }

    public class PaginationHeader
    {
        public int CurrentPage { get; set; }
        public int DisplayItems { get; set; }
        public int ResultCount { get; set; }
        public long TotalItems { get; set; }
        public long TotalPages { get; set; }

        public PaginationHeader() { }
        public PaginationHeader(int currentPage, int displayItems, int resultCount, int totalItems, int totalPages)
        {
            this.CurrentPage = currentPage;
            this.DisplayItems = displayItems;
            this.ResultCount = resultCount;
            this.TotalItems = totalItems;
            this.TotalPages = totalPages;
        }
    }

    public class Messages
    {
        public const string ERR_001 = "{0} không tồn tại.";
        public const string ERR_002 = "Vui lòng nhập dữ liệu.";
        public const string ERR_003 = "Vui lòng chọn dữ liệu.";
        public const string ERR_004 = "Không thể kết nối đến server.";
        public const string ERR_005 = "Chưa nhập {0}";
        public const string ERR_006 = "{0} đã tồn tại.";
        public const string ERR_007 = "Production only. Chức năng đã bị chặn ở bản Development.";
        public const string ERR_Http_BadRequest = "BadRequest";
        public const string ERR_Http_UnAuthorized = "UnAuthorized";
        public const string ERR_Http_Error = "HttpErrorCode:{0}"; // nguyencuongcs 20181230 Có thể split : lấy vị trí thứ 2 để get http error code
        public const string ERR_Http_NoConnection = "NoConnectionCouldBeMade:{0}"; // nguyencuongcs 20181230 Có thể split : lấy vị trí thứ 2 để get domain
    }
}
