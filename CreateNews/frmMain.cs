﻿using Commons;
using Core;
using CoreFormatNews;
using Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreateNews
{
    public partial class frmMain : Form
    {
        TinTuc tt = new TinTuc();
        List<JObject> listGetNews = new List<JObject>();
        List<JObject> listMenus = new List<JObject>();

        public frmMain()
        {
            InitializeComponent();
            JArray arrlistGetNews = (JArray)Variables.data_master["listGetNews"];

            this.listGetNews = arrlistGetNews.ToObject<List<JObject>>();
            cbbGetNews.DataSource = this.listGetNews;
            cbbGetNews.DisplayMember = "Name";
            cbbGetNews.ValueMember = "Id";

            JArray arrlMenus = (JArray)Variables.data_master["lMenus"];
            this.listMenus = arrlMenus.ToObject<List<JObject>>();
            cbbMenus.DataSource = this.listMenus;
            cbbMenus.DisplayMember = "TenMenu";
            cbbMenus.ValueMember = "Id";
        }
        private void Main_Load(object sender, EventArgs e)
        {
            var selectedIndexGetNews = this.listGetNews.FindIndex(item => CommonMethods.ConvertToInt32(item["Id"]) == tt.GetNewsId);
            cbbGetNews.SelectedIndex = selectedIndexGetNews;
            var selectedIndexMenus = this.listMenus.FindIndex(item => CommonMethods.ConvertToInt32(item["Id"]) == tt.IdMenu);
            cbbMenus.SelectedIndex = selectedIndexMenus;
            dtpNgayUp.Format = DateTimePickerFormat.Custom;
            dtpNgayUp.CustomFormat = "yyyy/MM/dd HH:mm:ss";

        }
        private async void BtnLoadHtml_Click(object sender, EventArgs e)
        {
            
            if(txtPath.Text==string.Empty || !txtPath.Text.CheckURLValid())
            {
                MessageBox.Show("Đường dẫn không hợp lệ!");
                return;
            }
            try
            {
                //bool isExist = await CommonTinTucMethods.CheckExistLinkGoc(txtPath.Text);
                //if (isExist)
                //{
                //    MessageBox.Show("Link đã tồn tại!");
                //    return;
                //}
                FormatNews fn = new FormatNews();
                tt = fn.ConvertUrlToTinTuc(txtPath.Text);
                this.BinddingData();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }


        }
        private void BinddingData()
        {

            cbbGetNews.SelectedIndex = cbbGetNews.Items.Cast<JObject>().ToList().FindIndex(item => item["Id"].ToObject<int>() == tt.GetNewsId);
            txtTitle.Text = tt.TieuDeTinTuc;
            txtMoTa.Text = tt.MoTaNgan;
            txtContent.DocumentHTML = tt.Content;
            txtUrlHinhDaiDien.Text = tt.UrlHinhDaiDien;
            ptbHinhDaiDien.Image = CommonMethods.resizeImageFromURL(tt.UrlHinhDaiDien, 600, 400);
        }
        private async void BtnTao_Click(object sender, EventArgs e)
        {
            try
            {
                var itemGetNews = JObject.FromObject(this.cbbGetNews.SelectedItem);
                tt.GetNewsId = CommonMethods.ConvertToInt32(itemGetNews["Id"]);
                tt.TieuDeTinTuc = txtTitle.Text;
                tt.MoTaNgan = txtMoTa.Text;
                tt.Content = txtContent.DocumentHTML;
                tt.LinkGoc = txtPath.Text;
                tt.UrlHinhDaiDien = txtUrlHinhDaiDien.Text;

                List<string> componentDataTinTucList = new List<string>();
                if (ckbTinNoiBat.Checked)
                {
                    componentDataTinTucList.Add("1");

                }
                componentDataTinTucList.Add("49");
                tt.ComponentDataTinTucList = String.Join(", ", componentDataTinTucList.ToArray());
                var itemMenu = JObject.FromObject(this.cbbMenus.SelectedItem);
                tt.IdMenu = CommonMethods.ConvertToInt32(itemMenu["Id"]);
                tt.UrlPrefix = CommonMethods.ConvertToString(itemMenu["UrlPrefix"]);
                tt.NgayUp = this.dtpNgayUp.Value;
                tt = await CommonTinTucMethods.Create(tt);
                System.Diagnostics.Process.Start("https://dailyxe.com.vn/admin/cms-tintuc-quanly/"+tt.Id);
                //this.BinddingData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
           

        }

        private void TxtUrlHinhDaiDien_TextChanged(object sender, EventArgs e)
        {
            tt.UrlHinhDaiDien = txtUrlHinhDaiDien.Text;
            ptbHinhDaiDien.Image = CommonMethods.resizeImageFromURL(tt.UrlHinhDaiDien, 600, 400);
        }

        private void HtmlEditControl1_Load(object sender, EventArgs e)
        {

        }
    }
}
