﻿using Commons;
using CreateNews;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static CreateNews.Variables;

namespace Core
{
    public class CoreHttp
    {
        private static readonly HttpClient _Client = new HttpClient();

        //Hàm gửi api base
        private static async Task<HttpResponseMessage> Request(HttpMethod pMethod, string pUrl, object pJsonContent, Dictionary<string, string> pHeaders, string mediaType = ContentTypeKeys.Json)
        {
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Method = pMethod;
            httpRequestMessage.RequestUri = new Uri(pUrl);


            if (pHeaders != null)
            {

                foreach (var head in pHeaders)
                {
                    if (head.Key != "Content-Type")
                    {
                        httpRequestMessage.Headers.Add(head.Key, head.Value);
                    }
                }
            }
            string authorization = string.Empty;
            try
            {
                authorization = httpRequestMessage.Headers.GetValues("Authorization").ToString();
            }
            catch (Exception)
            {

            }
            if (string.IsNullOrEmpty(authorization))
            {
                string _tokenIdAdmin = string.IsNullOrEmpty(Variables.token)? await CommonMethods.GetToken(): Variables.token;
                httpRequestMessage.Headers.Add("Authorization", "bearer " + _tokenIdAdmin);
            }
            _Client.DefaultRequestHeaders.Accept.Clear();
            //_Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));

            if (pMethod != HttpMethod.Get && pMethod != HttpMethod.Delete)
            {
                if (mediaType == ContentTypeKeys.FormData || mediaType == ContentTypeKeys.FormUrlencoded)
                {

                    var httpContent = new FormUrlEncodedContent(pJsonContent as Dictionary<string, string>);
                    httpRequestMessage.Content = httpContent;

                }
                else
                {
                    string strJson = CommonMethods.ConvertToString(pJsonContent);
                    strJson = strJson == null || strJson.Length == 0 ? "" : strJson;
                    var httpContent = new StringContent(strJson, Encoding.UTF8, "application/json");
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
                    httpRequestMessage.Content = httpContent;


                }


            }





            return await _Client.SendAsync(httpRequestMessage);
        }

        public static async Task<HttpResponseMessage> request_Get(string pUrl, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {

            return await Request(HttpMethod.Get, pUrl, null, pHeaders, mediaType);
            //HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentTypeKeys.Json));
            //return await client.GetAsync(pUrl);  // Blocking call!  
        }

        public static async Task<HttpResponseMessage> request_Put(string pUrl, object pJsonContent, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {

            return await Request(HttpMethod.Put, pUrl, pJsonContent, pHeaders, mediaType);
        }

        public static async Task<HttpResponseMessage> request_Post(string pUrl, object pJsonContent, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {

            return await Request(HttpMethod.Post, pUrl, pJsonContent, pHeaders, mediaType);
        }

        public static async Task<HttpResponseMessage> request_Delete(string pUrl, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {

            return await Request(HttpMethod.Delete, pUrl, null, pHeaders);
        }

    }
}
