﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Commons;
using CreateNews;
using Entity;
using HtmlAgilityPack;

namespace CoreFormatNews {
    public class ListNguons {
        public const string autopro = "autopro.com.vn";
        public const string zing = "zingnews.vn";
        public const string vnexpress = "vnexpress.net";
        public const string oto = "oto.com.vn";
        public const string xehay = "xehay.vn";
        public const string autodaily = "autodaily.vn";
        public const string danviet = "danviet.vn";
    }
     public class FormatNews
    {
        string patternYouTube = "<iframe width='100%' height='408' src='{0}' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>\n";
        string patternImageFigure = "<figure class='figure fluid'><img class='figure-img' src='{0}' style='width: 100%;'/></figure>";
        string patternImageFigcaption = "<figcaption>{0}</figcaption>";
        string patternItemGallery = "<div class='gallery-item'><a data-fancybox='gallery' href='{0}'><img class='figure-img fanciedbox' src='{0}'/> </a></div>";
        string patternImageCompcare = "<div class='splitter-container'><img class='figure-img' src='{0}'/> <img  class='figure-img' src='{1}' /></div>";
        public String FormatImage(String src, String caption = null)
        {
            String htmlImage = String.Format(patternImageFigure, src);
            HtmlNode imageFigure = HtmlNode.CreateNode(htmlImage);
            if (!String.IsNullOrEmpty(caption))
            {
                caption = caption.Trim();
            }
            if (!String.IsNullOrEmpty(caption) && caption.Length > 0)
            {
                imageFigure.AppendChild(HtmlNode.CreateNode(String.Format(patternImageFigcaption, caption)));
            }
            return imageFigure.OuterHtml;
        }
        public String FormatVideo(HtmlNode iframe)
        {
            if (iframe != null && !string.IsNullOrEmpty(iframe.Attributes["src"].Value) && iframe.Attributes["src"].Value.IndexOf("www.youtube.com") >= 0)
            {

                return $"{string.Format(patternYouTube, iframe.Attributes["src"].Value)}";
            }
            return string.Empty;
        }

        public String FormatNode(HtmlNode node)
        {

            node.Attributes["class"]?.Remove();
            //node.Attributes["style"]?.Remove();
            var a = node.SelectSingleNode(".//a");
            if (a != null && a.InnerText.ToLower().StartsWith("tại đây")) return string.Empty;
            return node?.OuterHtml;

        }
        public String FormatSliderCompare(HtmlNode node)
        {
            var lpic = node.SelectNodes(".//img");
            if (lpic == null) return string.Empty;
            string src1 = GetSrcByImage(lpic[0]);
            if (lpic.Count == 1) return FormatImage(src1);
            if (lpic.Count > 1)
            {
                string src2 = GetSrcByImage(lpic[1]);
                return $"{string.Format(patternImageCompcare, src1, src2)}";
            }
            return string.Empty;
        }
        public Boolean CheckNode(HtmlNode node)
        {

            if (node.SelectSingleNode(".//a") != null)
            {

                if (node.InnerText.ToLower().StartsWith("xem thêm") ||
                    node.InnerText.ToLower().StartsWith("đọc thêm") ||
                    node.InnerText.ToLower().EndsWith("tại đây") ||
                    node.InnerText.ToLower().EndsWith("tại đây.") ||
                    node.InnerText.ToLower().EndsWith("xin cảm ơn!")) return false;
            }
            if (node.InnerText.ToLower().EndsWith("xin cảm ơn!")) return false;

            return true;

        }
        public String FormatTable(HtmlNode table)
        {
            table.Attributes["class"]?.Remove();
            table.Attributes["style"]?.Remove();
            table.SetAttributeValue("class", "table table-bordered table-striped");
            table.SetAttributeValue("style", "margin-bottom:0");
            var ths = table.SelectNodes(".//th");
            if (ths != null)
                foreach (var th in ths)
                {
                    th.Attributes["class"]?.Remove();
                }
            var tds = table.SelectNodes(".//td");
            if (tds != null)
                foreach (var td in tds)
                {
                    td.Attributes["class"]?.Remove();
                    td.Attributes["style"]?.Remove();

                }
            return table?.OuterHtml;
        }
        public String GetSrcByImage(HtmlNode pic)
        {
            var src = pic.GetAttributeValue("data-origin", "");
            if(string.IsNullOrEmpty(src) || src.Length == 0)
            {
                src = pic.GetAttributeValue("data-src", "");
            }
            if (string.IsNullOrEmpty(src) || src.Length == 0)
            {
                src = pic.GetAttributeValue("src", "");
            }
            return src;
        }
        public String FormatGallery(HtmlNodeCollection pics, string caption = null)
        {
            if (pics == null) return string.Empty;
            if (pics.Count == 1)
            {
                var pic = pics[0];
                var src = GetSrcByImage(pic);
                return FormatImage(src, caption);

            }
            HtmlNode htmlDoc = HtmlNode.CreateNode("<div class='gallery'></div>");
            foreach (var pic in pics)
            {
                var src = GetSrcByImage(pic);
                htmlDoc.AppendChild(HtmlNode.CreateNode(String.Format(patternItemGallery, src)));
            }
            if (!String.IsNullOrEmpty(caption))
            {
                caption = caption.Trim();
            }
            if (!String.IsNullOrEmpty(caption) && caption.Length > 0)
            {
                htmlDoc.AppendChild(HtmlNode.CreateNode(String.Format("<p class='gallery-caption'>{0}</p>", caption)));
            }

            return htmlDoc?.OuterHtml;
        }
        public String GetUrlHinhDaiDien(HtmlNode body)
        {
            try
            {
                var pic = body.SelectSingleNode("//img");

                if (pic != null)
                {
                    return GetSrcByImage(pic);
                }
                else
                {
                    if(body.InnerHtml.IndexOf("youtube.com")>0)
                    {
                        var idYTB = Regex.Match(body.InnerHtml, @"(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&""'>]+)",
                        RegexOptions.IgnoreCase).Groups[5].Value;
                        if(!string.IsNullOrEmpty(idYTB) && idYTB.Length>0){
                            return string.Format("https://i.ytimg.com/vi/{0}/maxresdefault.jpg", idYTB);

                        }
                    }
                    
                }
            }
            catch (Exception)
            {

            }
            return "";

        }

        public string GetInnerText(HtmlNode htmlNode, string xpath)
        {
            try
            {
                return htmlNode.SelectSingleNode(xpath)?.InnerText;

            }
            catch (System.Exception)
            {

            }
            return string.Empty;
        }

        public TinTuc ConvertUrlToTinTuc(string url)
        {
            TinTuc tinTuc = new TinTuc();
            tinTuc.LinkGoc = url;
            Uri uri = new Uri(url);
            string host = uri.Host; // host is "www.contoso.com"
            switch (host)
            {
                //autopro
                case ListNguons.autopro:
                    tinTuc.GetNewsId = 1;
                    tinTuc = this.convertTinTucByAutopro(tinTuc);
                    break;
                //vnexpress
                case ListNguons.vnexpress:
                    tinTuc.GetNewsId = 2;
                    tinTuc = this.convertTinTucByVnexpress(tinTuc);
                    break;
                // //vnexpress

                // //https://oto.com.vn == bán xe hoi
                case ListNguons.oto:
                    tinTuc.GetNewsId = 8;
                    tinTuc = this.convertTinTucByOto(tinTuc);
                    break;
                //zing
                // case 6:
                case ListNguons.xehay:
                    tinTuc.GetNewsId = 15;
                    tinTuc = this.convertTinTucByXeHay(tinTuc);
                    break;
                // //zing
                case ListNguons.zing:
                    tinTuc.GetNewsId = 9;
                    tinTuc = this.convertTinTucByZing(tinTuc);
                    break;
                // case 10:
                // case 13:
                case ListNguons.autodaily:
                    tinTuc.GetNewsId = 14;
                    tinTuc = this.convertTinTucByAutodaily(tinTuc);
                    break;
                case ListNguons.danviet:
                    tinTuc.GetNewsId = 11;
                    tinTuc = this.convertTinTucByDanViet(tinTuc);
                    break;

                default:
                    tinTuc.GetNewsId = 17;
                    tinTuc = this.convertTinTucByDailyXe(tinTuc);
                    break;
            }
            HtmlDocument htmlDoc = new HtmlDocument();
            string urlResponse = CommonMethods.URLRequest(url);
            tinTuc.Content = tinTuc.Content.Replace("<p>&nbsp;</p>", string.Empty);

            htmlDoc.LoadHtml(tinTuc.Content);
            var nodesSpan = htmlDoc.DocumentNode
                .SelectNodes("//span")?
                .ToList() ?? new List<HtmlNode>() { };

            foreach (var node in nodesSpan)
            {
                if (node.ParentNode != null)
                {
                    node.ParentNode.InnerHtml = node.ParentNode.InnerText;

                }
            }
            var nodesToRemove = htmlDoc.DocumentNode
                .SelectNodes("//a")?
                .ToList() ?? new List<HtmlNode>() { };

            foreach (var node in nodesToRemove)
            {
                if (node.GetAttributeValue("data-fancybox", "") != "gallery")
                {

                    if (node.ParentNode != null)
                    {
                        node.ParentNode.InnerHtml = node.ParentNode.InnerText;

                    }
                }
            }
            tinTuc.Content = htmlDoc.DocumentNode.OuterHtml;
            tinTuc.UrlHinhDaiDien = GetUrlHinhDaiDien(htmlDoc.DocumentNode);
            return tinTuc;
        }

        public HtmlDocument GetHtmlDocument(string url)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            string urlResponse = CommonMethods.URLRequest(url);
            htmlDoc.LoadHtml(urlResponse);
            return htmlDoc;
        }

        public TinTuc GetTinTuc(TinTuc tinTuc, string xpathTieuDe, string xpathMoTa, string xpathBody, out HtmlNode body)
        {
            HtmlDocument htmlDoc = GetHtmlDocument(tinTuc.LinkGoc);

            //loại bỏ các thẻ a tránh trỏ link bậy
            tinTuc.TieuDeTinTuc = GetInnerText(htmlDoc.DocumentNode, xpathTieuDe)?.Trim();
            tinTuc.MoTaNgan = CommonMethods.HtmlMinify(GetInnerText(htmlDoc.DocumentNode, xpathMoTa).Trim());
            body = htmlDoc.DocumentNode.SelectSingleNode(xpathBody);
            //Convert the Raw HTML into an HTML Object
            return tinTuc;
        }

        private TinTuc convertTinTucByAutopro(TinTuc tinTuc)
        {

            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//h1[@class='title']", "//h2[@class='sapo']", "//div[@class='content-news-detail']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "div", "table", "ul" });
            List<string> lNotNodeName = new List<string>(new string[] { });
            // dạng 1
            List<HtmlNode> childNodes = body.ChildNodes.Where(node => lNodeName.IndexOf(node.Name) >= 0).ToList();
            var index = 0;
            foreach (HtmlNode item in childNodes)
            {
                switch (item.Name)
                {
                    case "table":
                        {
                            strHTML += FormatTable(item) + "\n";
                        }
                        break;
                    case "p":
                        if (!CheckNode(item)) continue;

                        if (index == (childNodes.Count - 1))
                        {
                            HtmlNode preNode = null;
                            if (index > 0) preNode = childNodes[index - 1];
                            if (preNode.Name == "p") continue;
                        }
                        if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                        {
                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                        }
                        else
                        {

                            strHTML += FormatNode(item) + "\n";

                        }

                        break;
                    case "div":
                        {
                            var type = item.GetAttributeValue("type", "");
                            if (type.IndexOf("Photo") >= 0)
                            {

                                var pic = item.SelectSingleNode(".//img");
                                if (pic != null)
                                {
                                    var a = item.SelectSingleNode(".//a");
                                    if (a != null && a.GetAttributeValue("href", "").IndexOf("https://autopro.com.vn/") >= 0) continue;
                                    var src = GetSrcByImage(pic);
                                    if (src.IndexOf("banner-autopro") >= 0) continue;
                                    var caption = item.SelectSingleNode(".//div[@class='PhotoCMS_Caption']")?.InnerText.Trim();
                                    strHTML += FormatImage(src, caption) + "\n";
                                }

                            }

                            if (type.IndexOf("LayoutAlbum") >= 0)
                            {
                                var caption = item.InnerText;
                                var lpic = item.SelectNodes(".//img");
                                strHTML += FormatGallery(lpic, caption) + "\n";
                            }
                            if (type.IndexOf("insertembedcode") >= 0)
                            {
                                var iframe = item.SelectSingleNode(".//iframe");
                                if (iframe != null)
                                {
                                    strHTML += FormatVideo(iframe) + "\n";
                                }
                            }

                        }
                        break;
                    default:
                        strHTML += FormatNode(item) + "\n";
                        break;
                }

                index++;

            };
            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByVnexpress(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//div[@class='sidebar-1']/h1", "//p[@class='description']", "//article[contains(@class,'fck_detail')]", out body);
            if (String.IsNullOrEmpty(tinTuc.TieuDeTinTuc))
            {
                tinTuc.TieuDeTinTuc = GetInnerText(body, "//h1[@class='title-detail']");

            }
            var description = body.SelectSingleNode("//p[@class='description']");
            description.SelectSingleNode(".//span")?.Remove();
            tinTuc.MoTaNgan = description?.InnerText;
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "div", "table", "figure", "ul" });
            List<string> lNotNodeName = new List<string>(new string[] { });
            var childNodes = body.ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0).ToList();
            // dạng 1
            for (int i = 0; i < childNodes.Count; i++)
            {
                HtmlNode item = childNodes[i];
                switch (item.Name)
                {
                    case "table":
                        {
                            strHTML += FormatTable(item) + "\n";
                        }
                        break;
                    case "figure":
                        {
                            var pic = item.SelectSingleNode(".//img");
                            if (pic != null)
                            {
                                var src = GetSrcByImage(pic);
                                if (src.IndexOf("banner-autopro") >= 0) continue;
                                var caption = item.SelectSingleNode(".//figcaption")?.InnerText.Trim();
                                strHTML += FormatImage(src, caption) + "\n";
                            }
                        }
                        break;
                    case "p":
                        {
                            if (!CheckNode(item)) continue;

                            if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                            {
                                strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                            }
                            else
                            {

                                strHTML += FormatNode(item) + "\n";

                            }
                        }
                        break;
                    case "div":
                        {
                            var caption = item.InnerText;
                            var lpic = item.SelectNodes(".//img");
                            caption = item.SelectSingleNode(".//div[@class='desc_cation']")?.InnerText ?? caption;
                            strHTML += FormatGallery(lpic, caption) + "\n";

                        }
                        break;
                    default:
                        if (item.GetAttributeValue("class", "").IndexOf("list-news") >= 0) continue;
                        strHTML += FormatNode(item) + "\n";
                        break;
                }

            }
            tinTuc.Content = strHTML;
            return tinTuc;
        }
        private TinTuc convertTinTucByOto(TinTuc tinTuc)
        {
            HtmlNode body = null;
            List<string> lNodeName = new List<string>();
            lNodeName = new List<string>(new string[] { "p", "h3", "h2", "div", "table", "ul" });

            try
            {
                tinTuc = GetTinTuc(tinTuc, "//h1[@class='title-detail']", "//*[@class='description']", "//div[contains(@class,'content-page')]", out body);
            }
            catch (Exception)
            {

                tinTuc = GetTinTuc(tinTuc, "//h2[contains(@class,'title')]", "//h2[contains(@class,'title')]", "//div[@class='content-detail-e']/div[@class='container']", out body);
            }
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNotNodeName = new List<string>(new string[] { });

            var childNodesroot = body.ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0).ToList();
            List<HtmlNode> childNodes = new List<HtmlNode>();

            foreach (var item in childNodesroot)
            {
                if (item.Name == "div" && item.GetAttributeValue("class", "").Contains("scroll-div"))
                {
                    childNodes.AddRange(item.ChildNodes);
                }
                else
                {
                    childNodes.Add(item);
                }
            }
            for (int i = 0; i < childNodes.Count; i++)
            {
                try
                {
                    HtmlNode item = childNodes[i];
                    switch (item.Name)
                    {

                        case "table":
                            {
                                var lpic = item.SelectNodes(".//img");
                                var ltd = item.SelectNodes(".//td");

                                if (lpic != null && (ltd?.Count() == lpic.Count()))
                                {
                                    strHTML += FormatGallery(lpic) + "\n";
                                }
                                else
                                {
                                    strHTML += FormatTable(item) + "\n";
                                }
                            }

                            break;
                        case "div":

                            if (item.GetAttributeValue("data-type", "") == "slider-compare")
                            {
                                strHTML += FormatSliderCompare(item) + "\n";
                            }
                            else
                            {
                                var div = item.SelectSingleNode(".//div");
                                if (div != null && div.GetAttributeValue("class", "").IndexOf("advantagesdefect") == -1) continue;

                                strHTML += FormatNode(item) + "\n";

                            }
                            break;
                        case "h2":
                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";
                            break;
                        case "h3":
                            strHTML += "<strong>" + item.InnerText + "</strong>" + "\n";
                            break;
                        case "p":
                            {
                                if (!CheckNode(item)) continue;
                                HtmlNode pic = item.SelectSingleNode(".//img");
                                if (pic == null)
                                {
                                    if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.GetAttributeValue("class", "").IndexOf("subtitle") >= 0 || (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null)))
                                    {
                                        strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                    }
                                    else
                                    {

                                        strHTML += FormatNode(item) + "\n";

                                    }
                                }
                                else
                                {
                                    var src = GetSrcByImage(pic);
                                    var caption = string.Empty;
                                    HtmlNode itemNext = childNodes[i];
                                    if (itemNext.Name == "p" && itemNext.InnerHtml.IndexOf("<em>") == 0)
                                    {
                                        caption = itemNext.InnerText;
                                        i++;
                                    }
                                    strHTML += FormatImage(src, caption) + "\n";

                                }
                            }

                            break;
                        default:
                            strHTML += FormatNode(item) + "\n";
                            break;
                    }
                }
                catch (System.Exception)
                {

                }

            }
            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByXeHay(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//h1[@class='single-title']", "//p[@class='description']", "//div[@class='news-content']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            Uri myUri = new Uri(tinTuc.LinkGoc);
            string hostName = myUri.ToString().Replace(myUri.PathAndQuery, "");
            List<string> lNodeName = new List<string>(new string[] { "p", "table", "iframe", "ul" });
            List<string> lNotNodeName = new List<string>(new string[] { });
            var childNodes = body.SelectSingleNode("//div[@class='divfirst']").ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0).ToList();
            childNodes = childNodes.Concat(body.SelectSingleNode("//div[@class='divend']").ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0).ToList()).ToList();
            // dạng 1           
            try
            {
                for (int i = 0; i < childNodes.Count(); i++)
                {
                    HtmlNode item = childNodes[i];
                    try
                    {
                        switch (item.Name)
                        {
                            case "iframe":
                                {
                                    if (item != null && !string.IsNullOrEmpty(item.Attributes["src"].Value) && item.Attributes["src"].Value.IndexOf("www.youtube.com") >= 0)
                                    {

                                        strHTML += $"<p> {string.Format(patternYouTube, item.Attributes["src"].Value)} </p>";
                                    }
                                }
                                break;
                            case "table":
                                {
                                    strHTML += FormatTable(item) + "\n";
                                }
                                break;
                            case "p":
                                {
                                    var iframe = item.SelectSingleNode(".//iframe");
                                    if (iframe != null)
                                    {
                                        if (!string.IsNullOrEmpty(iframe.Attributes["src"].Value) && iframe.Attributes["src"].Value.IndexOf("www.youtube.com") >= 0)
                                        {

                                            strHTML += $"<p> {string.Format(patternYouTube, iframe.Attributes["src"].Value)} </p>";
                                        }
                                    }
                                    else
                                    {
                                        var lpic = item.SelectNodes(".//img");
                                        if (lpic != null && lpic.Count > 0)
                                        {
                                            if (lpic.Count == 1)
                                            {
                                                var pic = lpic[0];
                                                var caption = item.InnerText;
                                                HtmlNode itemNext = childNodes.Count > (i + 1) ? childNodes[i + 1] : null;
                                                if (itemNext != null && itemNext.Name == "p" && itemNext.FirstChild != null && itemNext.FirstChild.Name == "em")
                                                {
                                                    i++;
                                                    caption = itemNext.InnerText;
                                                }
                                                caption = caption ?? item.InnerText;
                                                if (pic != null)
                                                {
                                                    var src = GetSrcByImage(pic);
                                                    if (!string.IsNullOrEmpty(src) && src.IndexOf("http") != 0)
                                                    {
                                                        src = hostName + src;

                                                    };
                                                    strHTML += FormatImage(src, caption) + "\n";

                                                }
                                            }
                                            else
                                            {
                                                strHTML += FormatGallery(lpic) + "\n";
                                            }

                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(item.InnerText))
                                            {
                                                if (item.InnerText.IndexOf("Tuoitrethudo") == -1 && item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.GetAttributeValue("class", "").IndexOf("subtitle") >= 0 || (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null)))
                                                {
                                                    strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                                }
                                                else
                                                {

                                                    strHTML += FormatNode(item) + "\n";

                                                }

                                            }

                                        }
                                    }

                                }
                                break;
                            default:
                                strHTML += FormatNode(item) + "\n";
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                    }
                }

            }
            catch (Exception ex) { }
            tinTuc.Content = strHTML;
            return tinTuc;
        }
        private TinTuc convertTinTucByAutodaily(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//h1", "//div[@class='article-detail-hd']/p", "//div[@class='article-content']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "figure", "table", "ul" });
            List<string> lNotNodeName = new List<string>(new string[] { });
            var childNodes = body.ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0 || (item.Name == "div" && item.GetAttributeValue("class", "").Contains("mceTmpl"))).ToList();
            // dạng 1

            for (int i = 0; i < childNodes.Count(); i++)
            {
                HtmlNode item = childNodes[i];
                try
                {

                    switch (item.Name)
                    {
                        case "table":
                            strHTML += FormatTable(item) + "\n";
                            break;
                        case "figure":
                            {
                                var pic = item.SelectSingleNode(".//img");
                                if (pic != null)
                                {
                                    var src = GetSrcByImage(pic);
                                    var caption = item.SelectSingleNode(".//figcaption")?.InnerText;
                                    strHTML += FormatImage(src, caption) + "\n";

                                }
                            }

                            break;
                        case "p":
                            {
                                var iframe = item.SelectSingleNode(".//iframe");
                                if (iframe != null)
                                {
                                    if (!string.IsNullOrEmpty(iframe.Attributes["src"].Value) && iframe.Attributes["src"].Value.IndexOf("www.youtube.com") >= 0)
                                    {

                                        strHTML += $"<p> {string.Format(patternYouTube, iframe.Attributes["src"].Value)} </p>";
                                    }
                                }
                                else
                                {
                                    var lpic = item.SelectNodes(".//img");
                                    if (lpic != null && lpic.Count > 0)
                                    {
                                        if (lpic.Count == 1)
                                        {
                                            var pic = lpic[0];
                                            var caption = string.Empty;
                                            if (!String.IsNullOrEmpty(item.InnerText))
                                            {
                                                caption = item.InnerText;
                                                if (pic != null)
                                                {
                                                    var src = GetSrcByImage(pic);
                                                    strHTML += FormatImage(src, caption) + "\n";
                                                }
                                            }
                                            else
                                            {
                                                HtmlNode itemNext = childNodes.Count > (i + 1) ? childNodes[i + 1] : null;
                                                if (itemNext != null && itemNext.Name == "p" && ((itemNext.InnerText.Length > 1 && itemNext.InnerText.Length <= 99) || (itemNext.FirstChild != null && itemNext.FirstChild.Name == "em")))
                                                {
                                                    i++;
                                                    caption = itemNext.InnerText;
                                                }
                                                caption = caption ?? item.InnerText;
                                                if (pic != null)
                                                {
                                                    var src = GetSrcByImage(pic);
                                                    strHTML += FormatImage(src, caption) + "\n";
                                                }
                                            }

                                        }
                                        else
                                        {
                                            strHTML += FormatGallery(lpic) + "\n";

                                        }

                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(item.InnerText))
                                        {
                                            if (item.InnerText.IndexOf("autodaily") == -1 && item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.GetAttributeValue("class", "").IndexOf("subtitle") >= 0 || (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null)))
                                            {
                                                strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                            }
                                            else
                                            {

                                                strHTML += FormatNode(item) + "\n";

                                            }

                                        }

                                    }
                                }
                            }

                            break;
                        default:
                            strHTML += FormatNode(item) + "\n";
                            break;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            tinTuc.Content = strHTML;
            return tinTuc;
        }
        private TinTuc convertTinTucByZing(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//header/h1[@class='the-article-title']", "//section[@class='main']/p[@class='the-article-summary']", "//section[@class='main']/div[@class='the-article-body']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "table", "h3", "ul" });
            // dạng 1
            var childNodes = body.ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0).ToList();

            foreach (HtmlNode item in childNodes)
            {
                switch (item.Name)
                {
                    case "table":
                        {
                            if (item.GetAttributeValue("class", "").Contains("picture"))
                            {
                                try
                                {

                                    var lpic = item.SelectNodes(".//img");
                                    var caption = item.InnerText;
                                    strHTML += FormatGallery(lpic, caption) + "\n";

                                }
                                catch (Exception ex)
                                {

                                }

                            }
                            else
                            {
                                if (item.GetAttributeValue("class", "").IndexOf("article") == -1)
                                {
                                    strHTML += FormatTable(item) + "\n";
                                }

                            }
                        }
                        break;
                    case "h3":
                        {
                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                        }
                        break;
                    case "p":
                        {
                            if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                            {
                                strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                            }
                            else
                            {

                                strHTML += FormatNode(item) + "\n";

                            }
                        }
                        break;
                    default:
                        strHTML += FormatNode(item) + "\n";
                        break;
                }

            };
            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByDanViet(TinTuc tinTuc)
        {
            try
            {
                HtmlNode body = null;
                tinTuc = GetTinTuc(tinTuc, "//h1", "//div[@class='sapo']", "//div[@class='dt-content']/div[contains(@class,'entry-body')]", out body);
                string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
                List<string> lNodeName = new List<string>(new string[] { "div", "p", "h2", "h3", "figure", "table", "ul" });
                List<string> lNotNodeName = new List<string>(new string[] { });
                // dạng 1
                List<HtmlNode> childNodes = body.ChildNodes.Where(x => lNodeName.IndexOf(x.Name) >= 0).ToList(); // DẠNG NÀY HÌNH Ảnh sẽ chứa caption

                for (int i = 0; i < childNodes.Count; i++)
                {
                    HtmlNode item = childNodes[i];
                    //if (item.Name == "div") {
                    //    var nameclass = item.GetAttributeValue ("class", "");
                    //    if (nameclass.IndexOf (" dvg_photo_box") == -1 && nameclass.IndexOf (" table") == -1 && nameclass.IndexOf (" LayoutAlbumWrapper") == -1) {
                    //        continue;
                    //    }
                    //}
                    switch (item.Name)
                    {
                        case "table":
                            strHTML += FormatTable(item) + "\n";
                            break;
                        case "h2":
                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";
                            break;
                        case "h3":
                            strHTML += "<strong>" + item.InnerText + "</strong>" + "\n";
                            break;
                        case "p":
                            {
                                if (!CheckNode(item)) continue;

                                if (item.InnerHtml == "<br>") continue;
                                HtmlNode pic = item.SelectSingleNode(".//img");
                                if (pic == null)
                                {
                                    if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                                    {
                                        strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                    }
                                    else
                                    {

                                        strHTML += FormatNode(item) + "\n";

                                    }

                                }
                                else
                                {
                                    var src = GetSrcByImage(pic);
                                    strHTML += FormatImage(src) + "\n";

                                }

                            }
                            break;
                        case "figure":
                            {
                                var caption = string.Empty;
                                var lpic = item.SelectNodes(".//img");
                                for (int y = 0; y < lpic.Count; y++)
                                {
                                    var pic = lpic[y];
                                    var src = GetSrcByImage(pic);

                                    if (y == (lpic.Count - 1))
                                    {
                                        caption = item.InnerText;
                                    }
                                    strHTML += FormatImage(src, caption) + "\n";

                                }
                                break;
                            }

                        case "div":
                            {
                                if (item.GetAttributeValue("class", "").Contains("LayoutAlbumContent"))
                                {
                                    var caption = item.InnerText;
                                    var lpic = item.SelectNodes(".//img");
                                    strHTML += FormatGallery(lpic, caption) + "\n";

                                }
                                else
                                {
                                    if (item.GetAttributeValue("type", "") == "RelatedNewsBox") continue;
                                    var table = item.SelectSingleNode(".//table");
                                    if (table != null)
                                    {
                                        strHTML += FormatTable(table) + "\n";
                                    }
                                    else
                                    {

                                        var caption = string.Empty;
                                        var lpic = item.SelectNodes(".//img");
                                        strHTML += FormatGallery(lpic, caption) + "\n";

                                    }
                                }

                            }

                            break;
                        default:
                            strHTML += FormatNode(item) + "\n";
                            break;

                    }

                }
                tinTuc.Content = strHTML;

            }
            catch (System.Exception)
            {

            }

            return tinTuc;
        }
        private TinTuc convertTinTucByDailyXe_baogiaothong(TinTuc tinTuc)
        {
            string strHTML = string.Empty;
            try
            {
                HtmlNode body = null;
                tinTuc = GetTinTuc(tinTuc, ".//h1", "//h2/span[contains(@class,'clrTit')]", "//div[contains(@class,'content-body')]", out body);
                strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
                Uri myUri = new Uri(tinTuc.LinkGoc);
                string hostName = myUri.ToString().Replace(myUri.PathAndQuery, ""); // host is "www.contoso.com"

                if (body != null)
                {

                    List<string> imageList = new List<string>();
                    List<string> lNodeName = new List<string>(new string[] { "p", "div", "table", "ul" });
                    // dạng 1
                    List<HtmlNode> childNodes = body.ChildNodes.Where(x =>
                       lNodeName.IndexOf(x.Name) >= 0).ToList();
                    for (int i = 0; i < childNodes.Count;)
                    {
                        HtmlNode item = childNodes[i];
                        if (lNodeName.IndexOf(item.Name) >= 0)
                        {
                            switch (item.Name)
                            {
                                case "table":
                                    strHTML += FormatTable(item) + "\n";
                                    break;
                                case "p":
                                case "div":
                                    HtmlNode pic = item.SelectSingleNode(".//img");
                                    if (pic == null)
                                    {
                                        if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                                        {
                                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                        }
                                        else
                                        {

                                            strHTML += FormatNode(item) + "\n";

                                        }

                                    }
                                    else
                                    {
                                        var src = GetSrcByImage(pic);
                                        string caption = item.InnerText;
                                        strHTML += FormatImage(src, caption) + "\n";
                                    }
                                    break;
                                default:
                                    strHTML += FormatNode(item) + "\n";
                                    break;

                            }

                        }
                        i++;

                    }
                }
            }
            catch (Exception)
            {

                var htmlDoc = new HtmlDocument();
                var urlResponse = CommonMethods.URLRequest(tinTuc.LinkGoc.Replace(".html", "is191635.html"));
                //Convert the Raw HTML into an HTML Object
                htmlDoc.LoadHtml(urlResponse);
                var nodeContent = htmlDoc.DocumentNode.SelectNodes("//div[@class='slAlmImg']");
                for (int i = 0; i < nodeContent.Count; i++)
                {
                    var chidItem = nodeContent[i];
                    var pic = chidItem.ChildNodes["img"];
                    if (pic == null)
                    {
                        strHTML += "<p>" + chidItem.InnerText.Trim() + "</p>" + "\n";

                    }
                    else
                    {
                        var src = GetSrcByImage(pic);
                        var caption = chidItem.InnerText.Trim();
                        strHTML += FormatImage(src, caption) + "\n";
                    }
                }
            }

            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByDailyXe_tuoitre(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//div/h1[@class='article-title']", "//h2[@class='sapo']", "//div[@id='main-detail-body']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "div", "table", "ul" });
            List<string> lNotNodeName = new List<string>(new string[] { });
            var limg = body.SelectNodes(".//img");
            foreach (var pic in limg)
            {
                var src = GetSrcByImage(pic);
                if (!src.StartsWith("https:"))
                {
                    src = "https:" + src;
                }
                pic.SetAttributeValue("src", src);

            }
            List<HtmlNode> childNodes = body.ChildNodes.Where(x =>
               lNodeName.IndexOf(x.Name) >= 0).ToList();

            foreach (HtmlNode item in childNodes)
            {
                if (lNodeName.IndexOf(item.Name) >= 0)
                {
                    switch (item.Name)
                    {
                        case "table":
                            {
                                var pic = item.SelectSingleNode(".//img");
                                if (pic != null)
                                {
                                    var caption = item.InnerText;
                                    var src = GetSrcByImage(pic);

                                    strHTML += FormatImage(src, caption) + "\n";

                                }
                                else
                                {
                                    strHTML += FormatTable(item) + "\n";

                                }
                            }
                            break;
                        case "p":
                            {
                                if (!CheckNode(item)) continue;
                                var iframe = item.SelectSingleNode(".//iframe");
                                if (iframe != null)
                                {
                                    strHTML += FormatVideo(iframe) + "\n";
                                }
                                else
                                {
                                    if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                                    {
                                        strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                    }
                                    else
                                    {

                                        strHTML += FormatNode(item) + "\n";

                                    }
                                }

                            }

                            break;
                        case "div":
                            if (item.Attributes["type"].Value.IndexOf("Photo") >= 0)
                            {
                                var lClass = item.Attributes["class"];
                                if (lClass != null && lClass.Value.IndexOf("VCSortableInPreviewMode") >= 0 && !(lClass.Value.IndexOf("link-content-footer") >= 0))
                                {

                                    var lpic = item.SelectNodes(".//img");
                                    var linkCallout = item.SelectNodes(".//a[contains(@class,'link-callout')]");
                                    if (linkCallout == null && lpic != null && lpic.Count >= 1)
                                    {
                                        if (lpic.Count == 1)
                                        {
                                            var pic = lpic[0];
                                            var src = GetSrcByImage(pic);
                                            var caption = item.InnerText;
                                            if (caption != null && !string.IsNullOrEmpty(caption))
                                            {
                                                if (caption.Length < 500)
                                                {
                                                    strHTML += FormatImage(src, caption) + "\n";
                                                }
                                                else
                                                {
                                                    strHTML += FormatImage(src) + "\n";
                                                    strHTML += "<p>" + caption + "</p>" + "\n";

                                                }
                                            }
                                            else
                                            {
                                                if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                                                {
                                                    strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                                }
                                                else
                                                {

                                                    strHTML += FormatNode(item) + "\n";

                                                }

                                            }
                                        }
                                        else
                                        {

                                            strHTML += FormatGallery(lpic) + "\n";

                                        }

                                    }

                                }

                            }
                            break;
                        default:
                            strHTML += FormatNode(item) + "\n";
                            break;

                    }

                }

            };
            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByDailyXe_vietnamnet(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//h1", "//div[@id='ArticleContent']/div[contains(@class,'ArticleLead')]", "//div[@id='ArticleContent']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "table", "h2", "h3", "iframe", "center", "figure", "ul" });
            List<string> lNotNodeName = new List<string>(new string[] { "div" });
            List<HtmlNode> childNodes = body.ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0 && (item.Name!="p" || (item.Name=="p" && CheckNode(item)))).ToList();
            for (int i = 0; i < childNodes.Count; i++)
            {
                var item = childNodes[i];
                var itemnext = childNodes.Count > (i + 1) ? childNodes[i + 1] : null;

                switch (item.Name)
                {
                    case "iframe":
                        {
                            if (item != null && !string.IsNullOrEmpty(item.Attributes["src"].Value) && item.Attributes["src"].Value.IndexOf("www.youtube.com") >= 0)
                            {

                                strHTML += string.Format(patternYouTube, item.Attributes["src"].Value);
                            }
                        }
                        break;
                    case "figure":
                    case "center":
                        {
                            var pic = item.SelectSingleNode(".//img");
                            if (pic != null)
                            {
                                var src = GetSrcByImage(pic);
                                var caption = item.InnerText;
                                if (itemnext.Name == "p" && itemnext.GetAttributeValue("class", "").IndexOf("t-c") == 0)
                                {
                                    caption = itemnext.InnerText ?? caption;
                                    i++;
                                }
                                strHTML += FormatImage(src, caption) + "\n";
                            }

                        }

                        break;
                    case "table":
                        var pics = item.SelectNodes(".//img");
                        if (pics!=null)
                        {
                            var caption = item.InnerText;
                            strHTML += FormatGallery(pics, caption) + "\n";

                        }
                        else
                        {
                            strHTML += FormatTable(item) + "\n";

                        }
                        break;
                    case "h2":
                        strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";
                        break;
                    case "h3":
                        strHTML += "<p><strong>" + item.InnerText + "</strong></p>" + "\n";
                        break;

                    case "p":
                        if (itemnext!=null && item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                        {
                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                        }
                        else
                        {

                            strHTML += FormatNode(item) + "\n";

                        }

                        break;
                    default:
                        strHTML += FormatNode(item) + "\n";
                        break;
                }
            }

            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByDailyXe_dantri(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//h1[@class='dt-news__title']", "//div[@class='dt-news__body']/div[@class='dt-news__sapo']/h2", "//div[@class='dt-news__body']/div[@class='dt-news__content']", out body);
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "figure", "table", "ul" });
            List<HtmlNode> childNodes = body.ChildNodes.Where(x => lNodeName.IndexOf(x.Name) >= 0).ToList(); // DẠNG NÀY HÌNH Ảnh sẽ chứa caption
            foreach (var item in childNodes)
            {
                switch (item.Name)
                {
                    case "table":
                        strHTML += FormatTable(item) + "\n";
                        break;
                    case "p":
                        if (!CheckNode(item)) continue;

                        if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                        {
                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                        }
                        else
                        {

                            strHTML += FormatNode(item) + "\n";

                        }

                        break;
                    case "figure":
                        var lpic = item.SelectNodes(".//img");
                        if (lpic != null)
                        {
                            var caption = item.SelectSingleNode(".//figcaption")?.InnerText;
                            strHTML += FormatGallery(lpic, caption) + "\n";
                        }
                        break;
                    default:
                        strHTML += FormatNode(item) + "\n";
                        break;
                }

            }

            tinTuc.Content = strHTML;

            return tinTuc;
        }
        private TinTuc convertTinTucByDailyXe_cafeauto(TinTuc tinTuc)
        {
            HtmlNode body = null;
            tinTuc = GetTinTuc(tinTuc, "//h1", "//div[contains(@class,'sevenPostDes')]", "//div[@id='sevenBoxNewContentInfo']", out body);

            if (body == null || body.ChildNodes == null)
            {
                tinTuc = GetTinTuc(tinTuc, "//h1", "//div[contains(@class,'sevenPostDes')]", "//div[@class='sevenPostContentCus']", out body);
            }
            string strHTML = "<h2>" + tinTuc.MoTaNgan + "</h2>" + "\n";
            List<string> lNodeName = new List<string>(new string[] { "p", "table", "ul" });
            List<HtmlNode> childNodes = body.ChildNodes.Where(item => lNodeName.IndexOf(item.Name) >= 0).ToList();
            for (int i = 0; i < childNodes.Count; i++)
            {
                try
                {
                    var item = childNodes[i];
                    var itemnext = (i == (childNodes.Count - 1)) ? null : childNodes[i + 1];

                    switch (item.Name)
                    {
                        case "table":
                            strHTML += FormatTable(item) + "\n";
                            break;

                        case "p":
                            {
                                var iframe = item.SelectSingleNode(".//iframe");
                                if (iframe != null && !string.IsNullOrEmpty(iframe.Attributes["src"].Value) && iframe.Attributes["src"].Value.IndexOf("www.youtube.com") >= 0)
                                {

                                    strHTML += string.Format(patternYouTube, iframe.Attributes["src"].Value);
                                }
                                else
                                {
                                    item.Attributes["style"]?.Remove();
                                    HtmlNodeCollection lpic = item.SelectNodes(".//img");
                                    if (lpic == null)
                                    {
                                        if (!CheckNode(item)) continue;

                                        if (item.GetAttributeValue("style", "").IndexOf("text-align:right") == -1 && (item.ChildNodes?.Count == 1 && item.SelectSingleNode(".//strong") != null))
                                        {
                                            strHTML += "<h3>" + item.InnerText + "</h3>" + "\n";

                                        }
                                        else
                                        {

                                            strHTML += FormatNode(item) + "\n";

                                        }

                                    }
                                    else
                                    {
                                        string caption = item.InnerText;
                                        if ((string.IsNullOrEmpty(caption) || caption.Length == 0) && itemnext != null && itemnext.InnerText.ToLower().IndexOf(". ảnh:") > 0)
                                        {
                                            caption = itemnext.InnerText;
                                            i++;
                                        }
                                        strHTML += FormatGallery(lpic, caption) + "\n";

                                    }
                                }
                            }

                            break;
                        default:
                            strHTML += FormatNode(item) + "\n";
                            break;
                    }
                }
                catch (Exception)
                {

                }

            }

            tinTuc.Content = strHTML;

            return tinTuc;
        }

        private TinTuc convertTinTucByDailyXe(TinTuc tinTuc)
        {
            Uri uri = new Uri(tinTuc.LinkGoc);

            string host = uri.Host; // host is "www.contoso.com"
            if (host.EndsWith("tuoitre.vn"))
            {
                host = "tuoitre.vn";
            }
            switch (host)
            {
                case "xe.baogiaothong.vn":
                    tinTuc = this.convertTinTucByDailyXe_baogiaothong(tinTuc);
                    break;
                case "dantri.com.vn":
                    tinTuc = this.convertTinTucByDailyXe_dantri(tinTuc);
                    break;
                case "tuoitre.vn":
                    tinTuc = this.convertTinTucByDailyXe_tuoitre(tinTuc);
                    break;
                case "vietnamnet.vn":
                    tinTuc = this.convertTinTucByDailyXe_vietnamnet(tinTuc);
                    break;
                case "cafeauto.vn":
                    tinTuc = this.convertTinTucByDailyXe_cafeauto(tinTuc);
                    break;
            }

            tinTuc.Content += "<p style='text-align: right;'> Theo " + host + "</p>";
            return tinTuc;
        }

}