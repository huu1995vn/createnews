﻿using Commons;
using Entity;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CreateNews
{
    public static class ExtensionMethods
    {
        /* ========== String ========== */
        public static bool CheckURLValid(this string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult);
        }
        public static string NullIsEmpty(this String value)
        {
            return value == null ? string.Empty : value;
        }

        public static string ToASCII(this String value)
        {
            return CommonMethods.ConvertUnicodeToASCII(value);
        }

        public static string AddFirstSplash(this String value)
        {
            //if (!string.IsNullOrEmpty(value))
            //{
            //    return (value.StartsWith("/") ? value : ("/" + value));
            //}

            return value.AddFirstChar("/");
        }
        public static string AddLastSplash(this String value)
        {
            return value.AddLastChar("/");
        }
        public static string RemoveFirstSplash(this String value)
        {
            while (value.StartsWith("/"))
            {
                value = value.RemoveFirstChar("/");
            }
            return value;
        }
        public static string RemoveLastSplash(this String value)
        {
            while (value.EndsWith("/"))
            {
                value = value.RemoveLastChar("/");
            }
            return value;
        }
        public static string AddFirstChar(this String value, string charValue)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return (value.StartsWith(charValue) ? value : (charValue + value));
            }

            return value;
        }
        public static string AddLastChar(this String value, string charValue)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return (value.EndsWith(charValue) ? value : (value + charValue));
            }

            return value;
        }
        public static string RemoveFirstChar(this String value, string charValue)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return (value.StartsWith(charValue) ? value.Substring(1, value.Length - 1) : value);
            }

            return value;
        }
        public static string RemoveLastChar(this String value, string charValue = "")
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (string.IsNullOrEmpty(charValue))
                {
                    return value.Substring(0, value.Length - 1);
                }
                else
                {
                    return (value.EndsWith(charValue) ? value.Substring(0, value.Length - 1) : value);
                }
            }

            return value;
        }
        /* ========== DateTime ========== */
        public static DateTime NullIsMinValue(this DateTime? value)
        {
            return !value.HasValue ? DateTime.MinValue : value.Value;
        }

        public static T FirstOrNewObject<T>(this IEnumerable<T> source)
        {

            if (source == null)
            {
                return Activator.CreateInstance<T>();
            }

            T obj = source.FirstOrDefault();

            if (obj == null)
            {
                obj = Activator.CreateInstance<T>();
            }

            return obj;
        }

        public static T FirstOrNewObject<T>(this IEnumerable<T> source, Expression<Func<T, bool>> predicate)
        {

            if (source == null)
            {
                return Activator.CreateInstance<T>();
            }

            T obj = source.AsQueryable().Where(predicate).FirstOrDefault();

            if (obj == null)
            {
                obj = Activator.CreateInstance<T>();
            }

            return obj;
        }

        /* ========== object ========== */
        public static object GetValue(this object obj, string propertyName)
        {
            return CommonMethods.GetPropertyValue(obj, propertyName);
        }

        public static bool HasProperty(this object obj, string propertyName)
        {
            return CommonMethods.HasProperty(obj, propertyName);
        }

        public static PropertyInfo GetPropertyInfo(this object obj, string propertyName)
        {
            return CommonMethods.GetPropertyInfo(obj, propertyName);
        }

        public static Type ExGetType(this PropertyInfo obj)
        {
            if (obj == null)
            {
                return typeof(object);
            }
            else
            {
                try
                {
                    try
                    {
                        // avoide nullable type
                        return Nullable.GetUnderlyingType(obj.PropertyType) ?? obj.PropertyType.GetGenericArguments()[0];
                    }
                    catch
                    {
                        return Type.GetType("System." + obj.PropertyType.Name);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static bool ToBoolean(this object obj, bool? defaultValue)
        {
            return CommonMethods.ConvertToBoolean(obj, defaultValue).Value;
        }

        /* ========== Type ========== */
        public static bool IsNullableType(this Type type)
        {
            if (type == null)
            {
                return false;
            }

            return type.FullName.ToLower().Contains("nullable");
        }

        /* ========== Dictionanry ========== */
        public static object GetValueFromKey(this Dictionary<string, object> dic, string key, string defaultValue)
        {
            if (dic != null && dic.Keys.Contains(key))
            {
                return dic[key];
            }

            return defaultValue;
        }

        public static string GetValueFromKey_ReturnString(this Dictionary<string, object> dic, string key, string defaultValue)
        {
            if (dic != null && dic.Keys.Contains(key))
            {
                return CommonMethods.ConvertToString(dic[key]);
            }

            return defaultValue.NullIsEmpty();
        }

        public static int GetValueFromKey_ReturnInt32(this Dictionary<string, object> dic, string key, int defaultValue = -1)
        {
            if (dic != null && dic.Keys.Contains(key))
            {
                return CommonMethods.ConvertToInt32(dic.GetValueFromKey_ReturnString(key, ""), defaultValue);
            }

            return defaultValue;
        }

        public static decimal GetValueFromKey_ReturnDecimal(this Dictionary<string, object> dic, string key, decimal defaultValue = 0)
        {
            if (dic != null && dic.Keys.Contains(key))
            {
                return CommonMethods.ConvertToDecimal(dic.GetValueFromKey_ReturnString(key, ""), defaultValue).Value;
            }

            return defaultValue;
        }

        public static Dictionary<string, object> GetValueFromKey_ReturnDic(this Dictionary<string, object> dic, string key)
        {
            if (dic != null && dic.Keys.Contains(key))
            {
                return CommonMethods.DeserializeObject<Dictionary<string, object>>((dic.GetValueFromKey_ReturnString(key, "")));
            }

            return new Dictionary<string, object>();
        }

        public static IEnumerable<Dictionary<string, object>> GetValueFromKey_ReturnIEnumerableDic(this Dictionary<string, object> dic, string key)
        {
            if (dic != null && dic.Keys.Contains(key))
            {
                return CommonMethods.DeserializeObject<IEnumerable<Dictionary<string, object>>>((dic.GetValueFromKey_ReturnString(key, "")));
            }

            return new List<Dictionary<string, object>>();
        }

        /************** JObject  ***************/
        public static Dictionary<string, object> ToDictionary(this JObject obj)
        {
            if (obj == null)
            {
                return new Dictionary<string, object>();
            }
            var result = obj.ToObject<Dictionary<string, object>>();

            var JObjectKeys = (from r in result
                               let key = r.Key
                               let value = r.Value
                               where value.GetType() == typeof(JObject)
                               select key).ToList();

            var JArrayKeys = (from r in result
                              let key = r.Key
                              let value = r.Value
                              where value.GetType() == typeof(JArray)
                              select key).ToList();

            JArrayKeys.ForEach(key => result[key] = ((JArray)result[key]).Values().Select(x => ((JValue)x).Value).ToArray());
            JObjectKeys.ForEach(key => result[key] = ToDictionary(result[key] as JObject));

            return result;
        }
    }

}
