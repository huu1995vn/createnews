﻿using Core;
using CreateNews;
using Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Commons
{
    public static class CommonTinTucMethods
    {
        public static async Task<CustomResultType<JObject>> UploadImage(ImageFile img)
        {
            JObject body = new JObject();
            body["FolderId"] = CommonMethods.ConvertToInt32(img.FolderId, Variables.folderId);
            body["ImageUrl"] = CommonMethods.ConvertToString(img.ImageUrl);
            body["Width"] = CommonMethods.ConvertToInt32(img.Width);
            body["Height"] = CommonMethods.ConvertToInt32(img.Height);
            body["TieuDe"] = CommonMethods.ConvertToString(img.TieuDe);
            body["GhiChu"] = CommonMethods.ConvertToString(img.GhiChu);
            body["Name"] = CommonMethods.ConvertToString(img.Name);
            var x = JsonConvert.SerializeObject(body);
            var res = await CommonApiMethods.Post(Variables.pathApiFiles, body);
            return res;
        }
        public static async Task<bool> CheckExistLinkGoc(string linkGoc)
        {
            var path = $"{Variables.pathApiTinTuc}?linkGoc={linkGoc}&trangThai=1,2";
            CustomResultType<JObject> res = await CommonApiMethods.Get(path);
            return res.DataResult.Count() > 0;
        }
        public static async Task<CustomResultType<JObject>> CreateTinTuc(TinTuc tt)
        {
            JObject body = tt.ToObject();
            string path = $"{Variables.pathApiTinTuc}/PostTinTucWithContent";
            var res = await CommonApiMethods.Post(path, body);
            return res;
        }
        public static async Task PostESByIdTinTuc(int id)
        {
            
            if(!(id > 0))
            {
                return;
            }
            JObject bodyES = new JObject();
            bodyES["id"] = id;
            bodyES["ni"] = "appfiles_news";
            bodyES["it"] = "httppost";

            JObject body = new JObject();
            body["at64"] = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(bodyES));
            CommonApiMethods.Post(Variables.pathES, body);

        }

        public static async Task<CustomResultType<JObject>> CreateFileChiTiet(int idTinTuc, string content)
        {
            JObject body = new JObject();
            body["id"] = idTinTuc;            
            content = Regex.Replace(content, @"<p><figure", @"<figure");
            content = Regex.Replace(content, @"figure></p>", @"figure>");
            content = Regex.Replace(content, "<p></p>", "");
            body["noiDung"] = content;

            var res = await CommonApiMethods.Post(Variables.pathApiCommonfile_CreateFileChiTietTinTuc, body);
            return res;
        }
      
        public static async Task<TinTuc> Create(TinTuc tt)
        {
            
            // B0: Chuẩn hóa dữ liệu như rewriteUrl            
            tt.RewriteUrl = CommonMethods.GetRewriteString(tt.TieuDeTinTuc);
            // B1: Lưu file đại diện tỉ lệ 3:2

            ImageFile img = new ImageFile();
            img.ImageUrl = tt.UrlHinhDaiDien;
            img.Width = 600;
            img.Height = 400;
            img.Name = tt.RewriteUrl;
            var resImage = await CommonTinTucMethods.UploadImage(img);
            var image = resImage.DataResult.ElementAt(0);
            tt.IdFileDaiDien = CommonMethods.ConvertToInt32(image.GetValue("Id"));
            // B2: Tạo tintuc
            var resTinTuc = await CommonTinTucMethods.CreateTinTuc(tt);
            var idTinTuc = CommonMethods.ConvertToInt32(resTinTuc.DataResult.ElementAt(0).GetValue("Id"));
            tt.Id = idTinTuc;
            return tt;
        }
    }

}
