﻿using CreateNews;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Commons
{
    public class CommonMethods
    {
        public static string HtmlMinify(string html)
        {
            return Regex.Replace(html.Trim(), @"( |\t|\r?\n)\1+", "$1");
        }

        public static async Task<string> GetToken(string userName, string passWord)
        {
            try
            {
                string strToken = "";
                //tronghuu95 20192702 đơn giản hóa KeyCacheParams
                string co = "@!";//KeyCacheParams.coalesceString;
                string tongHopBien = CommonMethods.CoalesceParams(co, Variables.domainApiToken + userName);
                string keyCache = tongHopBien;//string.Format(KeyCacheParams.folderIdAdmin, tongHopBien);

                object cacheValue = MemoryCacheHelper.GetValue(keyCache);
                if (cacheValue != null)
                {
                    return CommonMethods.DeserializeObject<string>(cacheValue);
                }

                string path = Variables.pathApiToken;

                string requireRole = "dm";

                /* chưa mã hóa folderId cho dailyxe
                Dictionary<string, string> body = new Dictionary<string, string>();
                body.Add(CommonParams.username, Variables.AdminForMainDomainUserName);
                body.Add(CommonParams.password, Variables.AdminForMainDomainPassword);            

                string bodyEncode = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(body));
                var body64 = new Dictionary<string, object>() { };
                    body64.Add(CommonParams.tokenData64, bodyEncode);
                */

                //string strRes = await apiCore.GetAsyncReturnStringDeserialized(path, Variables.HttpType_Post, body64, null);
                string strRes = "";
                //ApiGetCore<string> apiCore = new ApiGetCore<string>();

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // nguyencuongcs 20181101: mã hóa
                    var plainAccount = new { sn = userName, aw = passWord, er = requireRole };
                    var body3DES = CommonMethods.Encrypt3DES(CommonMethods.SerializeObject(plainAccount), "");
                    var keyRSA = CommonMethods.EncryptRSAForApi(TrippleDESLib.Key);

                    var body = new FormUrlEncodedContent(new[]
                    {
                    new KeyValuePair<string, string>(CommonParams.tokenData, body3DES),
                    new KeyValuePair<string, string>(CommonParams.publicKey, keyRSA)
                    //new KeyValuePair<string, string>(CommonParams.tokenData, body3DES),
                    //new KeyValuePair<string, string>(CommonParams.publicKey, keyRSA)
                });

                    HttpResponseMessage httpRes = await client.PostAsync(path, body);
                    if (httpRes.IsSuccessStatusCode)
                    {
                        using (HttpContent content = httpRes.Content)
                        {
                            strRes = content.ReadAsStringAsync().Result;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(strRes))
                {
                    strToken = CommonMethods.GetTokenFromWholeObjectEcrypted(strRes);
                    Variables.token = strToken;
                    //Dictionary<string, object> res = CommonMethods.DeserializeObject<Dictionary<string, object>>(strRes);
                    //folderIdRes = res.GetValueFromKey_ReturnString(CommonParams.access_folderId, "");
                }

                // nguyencuongcs 20181204: bổ sung timespan expiry khoảng 2 - 3 tuền để xin lại folderId mới. Tránh bị hết hạn -> ko lấy được data interface
                TimeSpan days = new TimeSpan(15, 0, 0, 0); // 15 ngày
                                                           //await cacheController.AddCache(keyCache, CommonMethods.SerializeToJSON(folderIdRes), days);
                MemoryCacheHelper.Add(keyCache, CommonMethods.SerializeToJSON(strToken), DateTimeOffset.UtcNow.AddDays(15));

                return strToken;
            }
            catch (Exception ex)
            {
               
               
            }
            return "";
            
        }

        public static async Task<string> GetToken()
        {
            string userName = Variables.userNameAdmin;
            string passWord = Variables.passAdmin;
            return await CommonMethods.GetToken(userName, passWord);
        }
        //General Function to request data from a Server
        public static string URLRequest(string url)
        {
            string html = string.Empty;

            try
            {
                WebRequest req = HttpWebRequest.Create(url);
                req.Method = "GET";

                string source;
                using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    html = reader.ReadToEnd();
                }

               
            }
            catch (Exception)
            {
                using (var myWebClient = new WebClient())
                {
                    myWebClient.Headers["User-Agent"] = "MOZILLA/5.0 (WINDOWS NT 6.1; WOW64) APPLEWEBKIT/537.1 (KHTML, LIKE GECKO) CHROME/21.0.1180.75 SAFARI/537.1";

                    html = myWebClient.DownloadString(url);
                }

            }
            if(string.IsNullOrEmpty(html) || !new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>").IsMatch(html))
            {
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                html = response.Content;
            }
            return CommonMethods.HtmlMinify(WebUtility.HtmlDecode(html));
        }

        public static Image resizeImageFromURL(String OriginalFileURL, int heigth, int width, Boolean keepAspectRatio = false, Boolean getCenter = false)
        {
            int newheigth = heigth;
            WebResponse response = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(OriginalFileURL);
                response = request.GetResponse();
            }
            catch
            {
                return (System.Drawing.Image)new Bitmap(1, 1);
            }
            Stream imageStream = response.GetResponseStream();

            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromStream(imageStream);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (keepAspectRatio || getCenter)
            {
                int bmpY = 0;
                double resize = (double)FullsizeImage.Width / (double)width;//get the resize vector
                if (getCenter)
                {
                    bmpY = (int)((FullsizeImage.Height - (heigth * resize)) / 2);// gives the Y value of the part that will be cut off, to show only the part in the center
                    Rectangle section = new Rectangle(new System.Drawing.Point(0, bmpY), new System.Drawing.Size(FullsizeImage.Width, (int)(heigth * resize)));// create the section to cut of the original image
                    Bitmap orImg = new Bitmap((Bitmap)FullsizeImage);//for the correct effect convert image to bitmap.
                    FullsizeImage.Dispose();//clear the original image
                    using (Bitmap tempImg = new Bitmap(section.Width, section.Height))
                    {
                        Graphics cutImg = Graphics.FromImage(tempImg);//              set the file to save the new image to.
                        cutImg.DrawImage(orImg, 0, 0, section, GraphicsUnit.Pixel);// cut the image and save it to tempImg
                        FullsizeImage = tempImg;//save the tempImg as FullsizeImage for resizing later
                        orImg.Dispose();
                        cutImg.Dispose();
                        return FullsizeImage.GetThumbnailImage(width, heigth, null, IntPtr.Zero);
                    }
                }
                else newheigth = (int)(FullsizeImage.Height / resize);//  set the new heigth of the current image
            }//return the image resized to the given heigth and width
            return FullsizeImage.GetThumbnailImage(width, newheigth, null, IntPtr.Zero);
        }


        #region "Variables"

        public static string patternA_Lower = "à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ";
        public static string patternA_Upper = "À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ";
        public static string patternE_Lower = "è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ";
        public static string patternE_Upper = "È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ";
        public static string patternI_Lower = "ì|í|ị|ỉ|ĩ";
        public static string patternI_Upper = "Ì|Í|Ị|Ỉ|Ĩ";
        public static string patternO_Lower = "ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ";
        public static string patternO_Upper = "Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ";
        public static string patternU_Lower = "ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ";
        public static string patternU_Upper = "Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ";
        public static string patternY_Lower = "ỳ|ý|ỵ|ỷ|ỹ";
        public static string patternY_Upper = "Ỳ|Ý|Ỵ|Ỷ|Ỹ";
        public static string patternD_Lower = "đ";
        public static string patternD_Upper = "Đ";

        public static IEnumerable<string> _lstDevelopmentDomainName = new List<string>(){
            "ceoquang.com",
            "localhost",
            "127.0.0",
            "192.168.0",
            "192.168.11",
            "local."
        };

        #endregion

        #region "Commons"

        //public static void CheckAdmin()
        //{
        //    InterfaceBLL bll = new InterfaceBLL();

        //    int checkByfolderId = bll.CheckByfolderId().Result;
        //    if (checkByfolderId < 1)
        //    {
        //        RedirectToLogin(null, null);
        //    }
        //}

        //public static void RedirectToLogin(RewriteContext rc, ProcessURL pu)
        //{
        //    if (rc == null)
        //    {
        //        rc = new RewriteContext();

        //        rc.HttpContext = new HttpContextAccessor().HttpContext;
        //    }

        //    if (pu == null)
        //    {
        //        pu = new ProcessURL(rc.HttpContext);
        //    }

        //    if (rc.HttpContext == null)
        //    {
        //        rc.HttpContext = new HttpContextAccessor().HttpContext;
        //    }

        //    rc.Redirect302(pu.BuildLinkPageLogin());
        //}

        public static string _writeLogFolderPath = ":\\Logs\\DailyXeApi\\";

        public static void WriteLog(string message, string diskName = "C", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = _writeLogFolderPath;
                }

                string text = diskName + folderPath;
                if (!System.IO.Directory.Exists(text))
                {
                    System.IO.Directory.CreateDirectory(text);
                }
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(text + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt", true);
                streamWriter.WriteLine("------------------------------------------------------------------------------------");
                streamWriter.WriteLine(System.DateTime.Now.ToLongTimeString() + ": " + message);
                streamWriter.WriteLine("------------------------------------------------------------------------------------");
                streamWriter.Close();
            }
            catch { }
        }

        public static void WriteLogElasticController(string message, string diskName = "C", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{_writeLogFolderPath}ElasticController\\";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }

        public static void WriteLogMemoryCacheController(string message, string diskName = "C", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{_writeLogFolderPath}MemoryCacheController\\";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogApiGetCore(string message, string diskName = "C", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{_writeLogFolderPath}ApiGetCore\\";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogRedirectPageError(string message, string diskName = "C", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{_writeLogFolderPath}RedirectPageError\\";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogFileDataCacheStaticHtml(string message, string diskName = "C", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{_writeLogFolderPath}FileDataCacheStaticHtml\\";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }



        public static string GetDomainByUrl(string pUrl)
        {
            Uri myUri = new Uri(pUrl);
            return myUri.Host;
        }

        public static string GetRandomString(int Length)
        {
            string Str = "abcdefghijklmnpqrtuvswzxy123456789";
            string Result = string.Empty;
            Random rand = new Random();
            for (int i = 0; i < Length; i++)
            {
                Result += Str[rand.Next(Str.Length)];
            }
            return Result;
        }

        public static int GetRandomNumber(int minVal = 4, int maxVal = 6)
        {
            int result = 0;

            Random ran = new Random();
            result = ran.Next(minVal, maxVal);

            if (result == 6)
            {
                result = 5;
            }

            return result;
        }

        public static string FomatChuHoaDauTu(string str)
        {
            string strResult = str;
            if (!string.IsNullOrEmpty(str))
            {
                str = str.ToLower();
                char[] charArr = str.ToCharArray();
                charArr[0] = Char.ToUpper(charArr[0]);
                foreach (System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(str, @"(\s\S)"))
                {
                    charArr[m.Index + 1] = m.Value.ToUpper().Trim()[0];
                }
                strResult = new String(charArr);
            }
            return strResult;
        }

        public static string GetRewriteString(string data)
        {
            string result = ConvertUnicodeToASCII(data.ToLower().Trim());

            result = System.Text.RegularExpressions.Regex.Replace(result, "[^a-z0-9 ]", " ");
            result = LoaiBoKhoangTrangThua(result);
            result = System.Text.RegularExpressions.Regex.Replace(result, "[ ]{1,}", "-");
            result = LoaiBoKyTuThua(result, "-");

            return result;
        }



        public static string GetRewriteTagString(string data)
        {
            string result = data.ToLower().Trim();

            result = System.Text.RegularExpressions.Regex.Replace(result, string.Format("[^a-z0-9{0}{1}{2}{3}{4}{5}{6} ]", patternA_Lower,
                        patternE_Lower, patternI_Lower, patternO_Lower, patternU_Lower, patternY_Lower, patternD_Lower), "");
            result = System.Text.RegularExpressions.Regex.Replace(result, "[ ]{1,}", "+");

            return result;
        }

        public static string RemovePrefixRewriteUrl(string pRewriteUrl)
        {
            string[] arrRewriteUrl = pRewriteUrl.Split('/');
            return arrRewriteUrl[arrRewriteUrl.Length - 1];
        }

        public static string LoaiBoKyTuThua(string text, string schar)
        {
            text = text.NullIsEmpty().Trim();
            text = System.Text.RegularExpressions.Regex.Replace(text, $"[{schar}]+", $"{schar}");
            return text;
        }

        public static string LoaiBoKhoangTrangThua(string text)
        {
            text = text.Trim();
            text = System.Text.RegularExpressions.Regex.Replace(text, @"[ ]+", " ");
            return text;
        }

        public static string LoaiBoKyTuODau(string text, string kyTu)
        {
            while (text.StartsWith(kyTu))
            {
                text = text.Substring(1, text.Length - 1);
            }

            return text;
        }

        public static string LoaiBoKyTuOCuoi(string text, string kyTu)
        {
            int length = 0;
            while (text.EndsWith(kyTu))
            {
                length = text.Length;
                text = text.Substring(0, length - 1);
            }

            return text;
        }

        public static string LoaiBoKyTuODauVaCuoi(string text, string kyTu)
        {
            text = LoaiBoKyTuODau(text, kyTu);
            text = LoaiBoKyTuOCuoi(text, kyTu);
            return text;
        }

        public static string LoaiBoKyTuDienThoai(string text)
        {
            return System.Text.RegularExpressions.Regex.Replace(text, "[^0-9]+", string.Empty);
        }

        public static string GetPhoneByString(string pValue)
        {
            string[] arrValues = pValue.Split('-');
            if (arrValues.Length > 0)
            {
                return LoaiBoKyTuDienThoai(arrValues[0]);
            }
            return LoaiBoKyTuDienThoai(pValue);
        }

        public static string FilterHtmlTag(string pValue)
        {
            return pValue.Replace("<", "&lt;").Replace(">", "&gt;");
        }

        public static string FilterHtmlTagMultiLanguage(string pValue)
        {
            pValue = FilterSpecialCharErrorXml(pValue);
            return FilterHtmlTag(pValue);
        }

        public static string FilterSpecialCharErrorXml(string pValue)
        {
            // & -> &amp;
            // < -> &lt;
            // > -> &gt;
            // " -> &quot;
            // ' -> &apos;
            string goodAmpersand = "&amp;";
            Regex badAmpersand = new Regex("&(?![a-zA-Z]{2,6};|#[0-9]{2,4};)");
            return badAmpersand.Replace(pValue, goodAmpersand);
        }

        public static string FilterNumber(string text)
        {
            return System.Text.RegularExpressions.Regex.Replace(text, "[^0-9]+", string.Empty);
        }

        public static string FilterTitleTag(string pValue)
        {
            return pValue.Replace("\"", "");
        }
        public static string FilterSpecialCharForElasticSearchQuery(string value)
        {
            string res = "";

            Regex badAmpersand = new Regex(string.Format("[^ A-za-z0-9{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}]",
                patternA_Lower, patternE_Lower, patternI_Lower, patternO_Lower, patternU_Lower, patternY_Lower, patternD_Lower,
                patternA_Upper, patternE_Upper, patternI_Upper, patternO_Upper, patternU_Upper, patternY_Upper, patternD_Upper));

            // result = System.Text.RegularExpressions.Regex.Replace(result, "[ ]{1,}", "+"); Thay khoảng trắng = dấu +
            res = badAmpersand.Replace(value, "");

            Regex badAmpersand2 = new Regex("[][]");// ][ bên trong [] là 2 ký tự đặc biệt của regex
            res = badAmpersand2.Replace(res, "");

            return res;
        }

        public static string ConvertUnicodeToASCII(string text)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.ToLower().Normalize(System.Text.NormalizationForm.FormD);
            string res = regex.Replace(strFormD, String.Empty).Replace('đ', 'd');
            return res;
        }

        public static Byte ConvertToByte(object pNumber)
        {
            try
            {
                return Convert.ToByte(pNumber);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToByte: " + error);
                return 0;
            }
        }

        public static Byte? ConvertToByte(object pNumber, Byte? defaultValue)
        {
            try
            {
                return (Byte?)Convert.ToByte(pNumber);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToByte: " + error);
                return defaultValue;
            }
        }

        public static int ConvertToInt32(object pNumber)
        {
            try
            {
                return Convert.ToInt32(CommonMethods.ConvertToString(pNumber));
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToInt32: " + error);
                return 0;
            }
        }

        public static int ConvertToInt32(object pNumber, int defaultValue = 0)
        {
            try
            {
                return Convert.ToInt32(CommonMethods.ConvertToString(pNumber));
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToInt32: " + error);
                return defaultValue;
            }
        }

        public static long ConvertToInt64(object pNumber)
        {
            try
            {
                return Convert.ToInt64(CommonMethods.ConvertToString(pNumber));
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToInt64: " + error);
                return 0;
            }
        }

        public static long? ConvertToInt64(object pNumber, long? defaultValue)
        {
            try
            {
                return Convert.ToInt64(CommonMethods.ConvertToString(pNumber));
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToInt64: " + error);
                return defaultValue;
            }
        }

        public static decimal ConvertToDecimal(object pNumber)
        {
            try
            {
                return Convert.ToDecimal(pNumber);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToDecimal: " + error);
                return 0;
            }
        }

        public static decimal? ConvertToDecimal(object pNumber, decimal? defaultValue)
        {
            try
            {
                return Convert.ToDecimal(pNumber);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToDecimal: " + error);
                return defaultValue;
            }
        }

        public static double ConvertToDouble(object pNumber)
        {
            try
            {
                return Convert.ToDouble(pNumber);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToDouble: " + error);
                return 0;
            }
        }

        public static string ConvertToString(object pString, string defaultValue = null)
        {

            try
            {
                return Convert.ToString(pString);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return string.IsNullOrEmpty(defaultValue)? string.Empty : defaultValue;
            }
        }

        public static string BuildIdBaiVietByActiveGroupStyle(object pString, object pActiveGroupStyle)
        {
            try
            {
                return Convert.ToString(pString + "-" + pActiveGroupStyle);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return string.Empty;
            }
        }


        public static DateTime ConvertToDateTime(string pDate)
        {
            return ConvertToDateTime(pDate, "dd-MM-yyyy");
        }

        public static ArrayList ConvertToArrayList(object pObject)
        {
            try
            {
                return (ArrayList)pObject;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToArrayList: " + error);
                return null;
            }
        }

        public static DateTime ConvertToDateTime(string pDate, string pFormat)
        {
            try
            {
                return DateTime.ParseExact(pDate, pFormat, null);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToDateTime: " + error);
                return DateTime.MinValue;
            }
        }

        public static DateTime ParseToDateTime(string pDate)
        {
            try
            {
                return DateTime.Parse(pDate);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ParseToDateTime: " + error);
                return DateTime.MinValue;
            }
        }

        public static DateTime ToDateTime(object pDate)
        {
            try
            {
                return Convert.ToDateTime(pDate);
            }
            catch
            {
                // nguyencuongcs 20181022: exception thường là do format trả về dạng ddMMyyyy -> sai culture mặc định là MMddyyyy                
                var format = "dd/MM/yyyy h:mm:ss tt"; // "dd/MM/yyyy"; // your datetime format            
                try
                {
                    DateTime newDataFormat = ConvertToDateTime(pDate.ToString(), format);
                    return newDataFormat;
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                    return DateTime.MinValue;
                }
            }
        }

        public static DateTime? ToDateTime(object pDate, DateTime? defaultValue)
        {
            try
            {
                return Convert.ToDateTime(pDate);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ToDateTime: " + error);
                return defaultValue;
            }
        }

        public static bool ConvertToBoolean(object pBoolean)
        {
            try
            {
                return Convert.ToBoolean(pBoolean);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToBoolean: " + error);
                return false;
            }
        }

        public static bool? ConvertToBoolean(object pBoolean, bool? defaultValue)
        {
            try
            {
                return Convert.ToBoolean(pBoolean);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToBoolean: " + error);
                return defaultValue;
            }
        }

        //public static string FormatDateTime(DateTime pDateTime)
        //{
        //    return FormatDateTime(pDateTime, Variables.DD_MM_YYYY);
        //}

        //public static string FormatDateTimeMMDDYYYY(DateTime? pDateTime)
        //{
        //    if (!pDateTime.HasValue)
        //    {
        //        return string.Empty;
        //    }

        //    return FormatDateTime(pDateTime.Value, Variables.MM_DD_YYYY);
        //}

        //public static string FormatDateTime_DD_MM_YYYY_FULL(DateTime pDateTime)
        //{
        //    return FormatDateTime(pDateTime, Variables.DD_MM_YYYY_FULL);
        //}

        public static string FormatDateTimeFull(DateTime pDateTime)
        {
            string format = "Ngày {0} tháng {1} năm {2} ";
            string Ngay = FormatNumber(pDateTime.Day, 2);
            string Thang = FormatNumber(pDateTime.Month, 2);
            return string.Format(format, Ngay, Thang, pDateTime.Year) + FormatDateTime(pDateTime, "HH:mm:ss");
        }

        public static string FormatDateTime(DateTime pDateTime, string pFormat)
        {
            try
            {
                return pDateTime.ToString(pFormat);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                WriteLog("ConvertToBoolean: " + error);
                return "";
            }
        }

        //public static string FormatDateTimeSEO(DateTime pDateTime)
        //{
        //    return FormatDateTime(pDateTime, Variables.YYYY_MM_DD_SEO);
        //}

        public static string ConvertTimeRangeToString(DateTime pDatetime) // nguyencuongcs-20170927: context phải truyền từ nơi gọi vào & != null
        {
            DateTime currentServerDatetime = DateTime.Now; // CommonMethods.GetServerDate(context);
            string res = string.Empty;
            if (pDatetime >= currentServerDatetime)
            {
                res = "0 phút";
                return res;
            }

            try
            {
                TimeSpan datediff = currentServerDatetime.Subtract(pDatetime);
                if (datediff != null)
                {
                    double temp = datediff.TotalMinutes;
                    res = Math.Round(temp, 0) + " phút";
                    if (temp >= 60) // 60 mins
                    {
                        temp = datediff.TotalHours;
                        res = Math.Round(temp, 0) + " giờ";

                        if (temp >= 24) // 24 hours
                        {
                            temp = datediff.TotalDays;
                            res = Math.Round(temp, 0) + " ngày";
                            if (temp >= 30) // 30 days
                            {
                                temp = (float)temp / float.Parse("30");
                                res = Math.Round(temp, 0) + " tháng";
                                if (temp >= 12)
                                {
                                    temp = (float)temp / float.Parse("12");
                                    res = Math.Round(temp, 0) + " năm";
                                }
                            }
                        }
                    }
                }

                //res = string.IsNullOrEmpty(res) ? res : res + " | " + CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(pDatetime) + " | " + CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(currentServerDatetime);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("ConvertTimeRangeToString: " + ex.ToString());
            }

            return res;
        }

        public static string FormatNumber(decimal pNumber)
        {
            return (pNumber != 0) ? pNumber.ToString("###,###") : "0";
        }

        public static string FormatNumber(decimal pNumber, string pSplitChar)
        {
            return (pNumber != 0) ? pNumber.ToString("###" + pSplitChar + "###") : "0";
        }

        public static string FormatNumber(int pNumber, int pLengthString)
        {
            return pNumber.ToString().PadLeft(pLengthString, '0');
        }

        public static string FormatGiaXe(string strGia)
        {
            double gia = CommonMethods.ConvertToDouble(strGia);

            if (gia == 0 || gia < 0)
            {
                return "0 đồng";
            }

            string[] sizes = new string[4] { "đồng", "ngàn", "triệu", "tỷ" };
            int k = 1000;
            int i = 0;

            try

            {
                i = (int)(Math.Floor(Math.Log(gia) / Math.Log(k)));
            }
            catch
            {
                i = 0;
            }

            return (gia / Math.Pow(k, i)) + " " + sizes[i];
        }

        public static string FilterTen(string pString)
        {
            pString = FilterHtmlTagMultiLanguage(pString);
            pString = LoaiBoKhoangTrangThua(pString);
            return pString;
        }

        public static string FormatMoTaNgan(string pString)
        {
            pString = pString.Replace("[b]", "<strong>").Replace("[/b]", "</strong>");
            return FormatHtmlEndLine(pString);
        }

        public static string FormatHtmlEndLine(string pString)
        {
            pString = pString.Replace(Environment.NewLine, "<br/>");
            pString = pString.Replace("\r\n", "<br/>");
            return pString;
        }

        public static string RemoveCharactersMoTaNgan(string pString)
        {
            pString = pString.Replace("[b]", "").Replace("[/b]", "");
            pString = pString.Replace(Environment.NewLine, ", ");
            pString = pString.Replace("\r\n", ", ");
            return pString;
        }

        public static string ReplaceCharactersToHtmlCode(string pString)
        {
            pString = pString.Replace("\"", "&#34;").Replace("\'", "&#39;").Replace(":", "&#58;");
            return pString;
        }

        public static string ModifyLengString(string Content, int MaxLeng)
        {
            if (Content.Length <= MaxLeng)
                return Content;
            string Temp = Content.Substring(0, MaxLeng);
            int spaceindex = Temp.LastIndexOf(' ');
            return Temp.Substring(0, spaceindex) + "...";
        }

        public static string RemoveAllHTMLTag(string HtmlContent)
        {
            HtmlContent = WebUtility.HtmlDecode(HtmlContent);
            string res = System.Text.RegularExpressions.Regex.Replace(HtmlContent, "<([^>]*)>", string.Empty);
            res = System.Text.RegularExpressions.Regex.Replace(res, "([ ]{2,})", " ");
            res = res.Replace(Environment.NewLine, string.Empty);
            res = res.Replace("\r\n", string.Empty);
            return res;
        }

        public static void DownloadFileByUrl(string pUrl, string pPathFile)
        {
            try
            {
                // WebClient webClient = new WebClient();
                // webClient.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36");
                // webClient.Headers.Add(HttpRequestHeader.Accept, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
                // webClient.Proxy = System.Net.IWebProxy.GetDefaultProxy();
                // webClient.DownloadFile(pUrl, pPathFile);
            }
            catch { }
        }

        public static Dictionary<String, String> ParseDomainList(string pDomainList)
        {
            Dictionary<String, String> lstResult = new Dictionary<String, String>();
            string[] lstDomain = pDomainList.Split('@');
            string[] lstTemp;
            foreach (string domain in lstDomain)
            {
                lstTemp = domain.Split('|');
                if (lstTemp.Length > 1)
                {
                    lstResult.Add(lstTemp[0].Trim().ToLower(), lstTemp[1].Trim().ToLower());
                }
            }
            return lstResult;
        }


        // public static int GetIDAdmin()
        // {
        //     return CommonMethods.ConvertToInt32(HttpContext.Current.Session[Variables.SADMIN_ID]);
        // }
        // public static bool GetIsRootAdmin()
        // {
        //     return CommonMethods.ConvertToBoolean(HttpContext.Current.Session[Variables.SADMIN_ISROOT]);
        // }

        public static string FormatNguoiVietWithMark(string pNguoiViet)
        {
            if (string.IsNullOrEmpty(pNguoiViet))
            {
                return string.Empty;
            }
            return /*"@" +*/ pNguoiViet;
        }


        public static async Task GetContentOfFileAsync(string url, string pathFile)
        {
            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    await DownloadAsync(url, pathFile);
                    //await DownloadGZipAsync(url, pathFile);
                }
                catch (Exception ex)
                {
                    string exString = ex.ToString();
                    CommonMethods.WriteLog("GetContentOfFile: " + ex.ToString());
                }
            }
        }



        public static async Task Download(string requestUri, string pathFile)
        {
            int bufferSize = 100 * 1024; // 10Mb

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (
                Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync(),
                stream = new FileStream(pathFile, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize, false))
            {
                contentStream.CopyTo(stream);
            }
        }

        public static async Task DownloadAsync(string requestUri, string pathFile)
        {
            int bufferSize = 100 * 1024; // 10Mb

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (
                Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync(),
                stream = new FileStream(pathFile, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize, true))
            {
                await contentStream.CopyToAsync(stream);
            }
        }

        public static async Task DownloadGZipAsync(string requestUri, string pathFile)
        {
            int bufferSize = 100 * 1024; // 10Mb

            using (
                Stream stream = new FileStream(pathFile, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize, true))
            {
                Byte[] b = System.Text.Encoding.ASCII.GetBytes(GetContentWebsite_GZip(requestUri).Result);
                Stream contentStream = new MemoryStream(b);
                await contentStream.CopyToAsync(stream);
            }
        }

        public static async Task<string> GetContentWebsite_GZip(string link)
        {
            string content = string.Empty;
            try
            {
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
                //request.AutomaticDecompression = DecompressionMethods.GZip;

                //// Right now... this is what our HTTP Request has been built in to...
                ///*
                //    GET http://google.com/ HTTP/1.1
                //    Host: google.com
                //    Accept-Encoding: gzip
                //    Connection: Keep-Alive
                //*/

                //// Wrap everything that can be disposed in using blocks... 
                //// They dispose of objects and prevent them from lying around in memory...
                //using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())  // Go query google
                //using (System.IO.Stream responseStream = response.GetResponseStream())               // Load the response stream
                //using (System.IO.StreamReader streamReader = new System.IO.StreamReader(responseStream))       // Load the stream reader to read the response
                //{
                //    content = streamReader.ReadToEnd(); // Read the entire response and store it in the siteContent variable
                //}

                HttpClientHandler handler = new HttpClientHandler();
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (var httpClient = new HttpClient(handler))
                {
                    var apiUrl = (link);

                    //setup HttpClient
                    httpClient.BaseAddress = new Uri(apiUrl);
                    //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //httpClient.GetStreamAsync

                    //make request
                    return await httpClient.GetStringAsync(apiUrl);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void WriteStream(Stream stream)
        {
            if (stream != null)
            {
                int bufferSize = 100 * 1024; // 10Mb
                byte[] buffer = new byte[bufferSize];
                int readed = 0;

                using (stream)
                {
                    do
                    {
                        readed = stream.Read(buffer, 0, bufferSize);
                        stream.WriteAsync(buffer, 0, readed);
                    }
                    while (readed > 0);
                }
            }
        }

        public static List<int> SplitStringToInt(string data, char splitChar = ',')
        {
            List<int> lstId = new List<int>();

            if (string.IsNullOrEmpty(data) || data.Length < 1)
            {
                return lstId;
            }

            try
            {
                string[] arrData = data.Split(splitChar);
                int temp = -1;

                foreach (string val in arrData)
                {
                    if (Int32.TryParse(val, out temp))
                    {
                        lstId.Add(temp);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstId;
        }

        public static List<long> SplitStringToInt64(string data, char splitChar = ',')
        {
            List<long> lstId = new List<long>();

            if (string.IsNullOrEmpty(data) || data.Length < 1)
            {
                return lstId;
            }

            try
            {
                string[] arrData = data.Split(splitChar);
                long temp = -1;

                foreach (string val in arrData)
                {
                    if (Int64.TryParse(val, out temp))
                    {
                        lstId.Add(temp);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstId;
        }

        public static void SetPropertyValue(object obj, string propertyName, object val)
        {
            try
            {
                if (obj != null)
                {
                    Type myType = obj.GetType();
                    if (myType != null)
                    {
                        PropertyInfo prop = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        if (prop != null)
                        {
                            prop.SetValue(obj, val);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string exString = ex.ToString();
                // throw ex;
            }
        }

        public static object GetPropertyValue(object obj, string propertyName)
        {
            object res = null;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    PropertyInfo prop = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    if (prop != null)
                    {
                        res = prop.GetValue(obj, null);
                    }
                }
            }
            return res;
        }

        public static PropertyInfo GetPropertyInfo(object obj, string propertyName)
        {
            PropertyInfo res = null;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    res = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                }
            }
            return res;
        }

        public static IEnumerable<string> GetPropertyNameList(object obj)
        {
            IEnumerable<string> res = null;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    res = GetPropertyNameList(myType);
                }
            }
            return res;
        }

        public static IEnumerable<string> GetPropertyNameList(Type type)
        {
            List<string> res = null;

            if (type != null)
            {
                PropertyInfo[] arrPropInfo = type.GetProperties();

                if (arrPropInfo != null && arrPropInfo.Length > 0)
                {
                    res = new List<string>();
                    foreach (PropertyInfo propInfo in arrPropInfo)
                    {
                        res.Add(propInfo.Name);
                    }
                }
            }

            return res;
        }

        public static bool HasProperty(object obj, string propertyName)
        {
            bool res = false;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    PropertyInfo prop = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    return prop != null;
                }
            }
            return res;
        }


        public static string SerializeObject(object jsonData)
        {
            try
            {
                return JsonConvert.SerializeObject(jsonData);
            }
            catch
            {
                return CommonMethods.ConvertToString(jsonData);
            }
        }

        public static T DeserializeObject<T>(object objDataAsJson)
        {
            string jsonString = "";
            var format = "dd/MM/yyyy HH:mm:ss"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

            try
            {
                jsonString = CommonMethods.ConvertToString(objDataAsJson);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
            }
            catch
            {
                try
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString, dateTimeConverter);

                }
                catch
                {
                    try
                    {
                        // Serialized lại trước khi Deserialized
                        jsonString = CommonMethods.SerializeObject(objDataAsJson);
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
                    }
                    catch
                    {
                        try
                        {
                            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString, dateTimeConverter);
                        }
                        catch (Exception ex)
                        {
                            CommonMethods.WriteLog("DeserializeObject error: " + ex.ToString());
                            CommonMethods.WriteLog("DeserializeObject jsonData: " + jsonString);
                            return (T)Activator.CreateInstance<T>();
                        }
                    }
                }
            }
        }

        public static T ExChangeType<T>(object value)
        {
            Type t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)value;
            // (value == null || DBNull.Value.Equals(value)) ?
            //    default(T) : (T)Convert.ChangeType(value, t);
        }

        public static string CoalesceParams(string coaChar, params object[] paramsData)
        {
            if (paramsData == null || paramsData.Count() < 1)
            {
                return "";
            }
            else
            {
                return string.Join(coaChar, paramsData);
            }
        }

        public static string CoalesceParamsWithoutDuplicateCoachar(string coaChar, params string[] paramsData)
        {
            if (paramsData == null || paramsData.Count() < 1)
            {
                return "";
            }
            else
            {
                List<string> newParams = new List<string>();
                if (paramsData != null && paramsData.Count() > 0)
                {
                    int length = 0;
                    string newItem = "";
                    foreach (string item in paramsData)
                    {
                        // Bỏ ký tự / ở đầu nếu có        
                        length = item.Length;
                        newItem = !item.StartsWith(coaChar) ? item : item.Substring(1, length - 1);

                        // Bỏ ký tự / ở cuối nếu có
                        length = newItem.Length;
                        newItem = !newItem.EndsWith(coaChar) ? newItem : newItem.Substring(0, length - 1);
                        if (!string.IsNullOrEmpty(newItem))
                        {
                            newParams.Add(newItem);
                        }
                    }
                }
                return string.Join(coaChar, newParams);
            }
        }

        #endregion

        #region "Get Path"

        public static string GetAppath(string appPath)
        {

            if (string.IsNullOrEmpty(appPath))
                appPath = "/";
            else if (!appPath.EndsWith("/"))
                appPath += "/";
            return appPath;
        }


        public static string SerializeToJSON(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            }
        }
        private static JsonSerializerSettings DefaultJsonSerializerSettings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Serialize
        };
        public static string SerializeToJSON(object obj, bool ignoreReferenceLoop = true)
        {
            JsonSerializerSettings settings = null;
            if (ignoreReferenceLoop)
            {
                settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };
            }
            else
            {
                settings = DefaultJsonSerializerSettings;
            }

            return SerializeToJSON(obj, settings);
        }

        public static string SerializeToJSON(object obj, JsonSerializerSettings settings)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {

                if (settings == null)
                {
                    settings = DefaultJsonSerializerSettings;
                }

                return JsonConvert.SerializeObject(obj, Formatting.Indented, settings);
            }
        }

        #endregion



        #region "Encrypt & Decrypt"

        public static string GetEncryptMD5(string pValue)
        {
            MD5 algorithm = MD5.Create();
            byte[] data = algorithm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(pValue));
            string result = "";
            for (int i = 0; i < data.Length; i++)
            {
                result += data[i].ToString("x2").ToUpperInvariant();
            }
            return result;
        }

        public static string EncodeTo64_ASCII(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64_ASCII(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncodeTo64_UTF8(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64_UTF8(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncodeTo64_UNICODE(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64_UNICODE(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncryptRSA(string text)
        {
            RSALib rsaLib = new RSALib();
            string res = rsaLib.Encrypt(text);
            return res;
        }

        public static string EncryptRSAForApi(string text)
        {
            RSALib rsaLib = new RSALib(true);
            string res = rsaLib.Encrypt(text);
            return res;
        }

        public static string DecryptRSA(string cipherText)
        {
            RSALib rsaLib = new RSALib();
            string res = rsaLib.Decrypt(cipherText);
            return res;
        }

        public static string Encrypt3DES(string text, string key)
        {
            string res = TrippleDESLib.Encrypt(text, key);
            return res;
        }

        public static string Decrypt3DES(string cipherText, string key)
        {
            string res = TrippleDESLib.Decrypt(cipherText, key);
            return res;
        }

        public static string GetTokenFromfolderIdObjectEcrypted(string encryptedData, string encryptedKey)
        {
            //Dictionary<string,object> dicEncryptedData = CommonMethods.DeserializeObject<Dictionary<string, object>>(encryptedData);
            //string cipherData = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.tokenData, "");
            //string cipherKey = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.folderId_key, "");

            string plainKey = DecryptRSA(encryptedKey);
            string plainData = Decrypt3DES(encryptedData, plainKey);

            Dictionary<string, object> dicfolderId = CommonMethods.DeserializeObject<Dictionary<string, object>>(plainData);

            var access_folderId = dicfolderId.GetValueFromKey_ReturnString(CommonParams.access_folderId, "");
            //var expires_in = rootJfolderId[CommonParams.expires_in] != null ? CommonMethods.ConvertToString(rootJfolderId[CommonParams.expires_in]) : string.Empty;
            //var admin = CommonMethods.DeserializeObject<List<Admin>>(rootJfolderId[CommonParams.admin]);

            return access_folderId;
        }

        public static string GetTokenFromWholeObjectEcrypted(string wholeObjectEcrypted) // {"at":"","uk":""}
        {
            Dictionary<string, object> dicEncryptedData = CommonMethods.DeserializeObject<Dictionary<string, object>>(wholeObjectEcrypted);
            string cipherData = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.tokenData, "");
            string cipherKey = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.publicKey, "");

            string plainKey = DecryptRSA(cipherKey);
            string plainData = Decrypt3DES(cipherData, plainKey);

            Dictionary<string, object> dicfolderId = CommonMethods.DeserializeObject<Dictionary<string, object>>(plainData);

            var access_folderId = dicfolderId.GetValueFromKey_ReturnString(CommonParams.access_folderId, "");
            //var expires_in = rootJfolderId[CommonParams.expires_in] != null ? CommonMethods.ConvertToString(rootJfolderId[CommonParams.expires_in]) : string.Empty;
            //var admin = CommonMethods.DeserializeObject<List<Admin>>(rootJfolderId[CommonParams.admin]);

            return access_folderId;
        }

        //public static Admin GetAdminFromfolderIdObjectEcrypted(string encryptedData, string encryptedKey)
        //{
        //    //Dictionary<string,object> dicEncryptedData = CommonMethods.DeserializeObject<Dictionary<string, object>>(encryptedData);
        //    //string cipherData = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.tokenData, "");
        //    //string cipherKey = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.folderId_key, "");

        //    string plainKey = DecryptRSA(encryptedKey);
        //    string plainData = Decrypt3DES(encryptedData, plainKey);

        //    Dictionary<string, object> dicfolderId = CommonMethods.DeserializeObject<Dictionary<string, object>>(plainData);

        //    Admin admin = CommonMethods.DeserializeObject<Admin>(CommonMethods.DecodeFrom64_ASCII(dicfolderId.GetValueFromKey_ReturnString(CommonParams.folderId_admin64, "")));
        //    //var expires_in = rootJfolderId[CommonParams.expires_in] != null ? CommonMethods.ConvertToString(rootJfolderId[CommonParams.expires_in]) : string.Empty;
        //    //var admin = CommonMethods.DeserializeObject<List<Admin>>(rootJfolderId[CommonParams.admin]);

        //    return admin;
        //}

        //public static Admin GetAdminFromWholeObjectEcrypted(string wholeObjectEcrypted) // {"at":"","uk":""}
        //{
        //    Dictionary<string, object> dicEncryptedData = CommonMethods.DeserializeObject<Dictionary<string, object>>(wholeObjectEcrypted);
        //    string cipherData = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.tokenData, "");
        //    string cipherKey = dicEncryptedData.GetValueFromKey_ReturnString(CommonParams.publicKey, "");

        //    string plainKey = DecryptRSA(cipherKey);
        //    string plainData = Decrypt3DES(cipherData, plainKey);

        //    Dictionary<string, object> dicfolderId = CommonMethods.DeserializeObject<Dictionary<string, object>>(plainData);

        //    Admin admin = CommonMethods.DeserializeObject<Admin>(CommonMethods.DecodeFrom64_ASCII(dicfolderId.GetValueFromKey_ReturnString(CommonParams.folderId_admin64, "")));
        //    //var expires_in = rootJfolderId[CommonParams.expires_in] != null ? CommonMethods.ConvertToString(rootJfolderId[CommonParams.expires_in]) : string.Empty;
        //    //var admin = CommonMethods.DeserializeObject<List<Admin>>(rootJfolderId[CommonParams.admin]);

        //    return admin;
        //}        

        public static bool CheckStartWith(List<string> lstStringToCheck, string rawUrl)
        {
            bool res = false;

            if (lstStringToCheck != null && !string.IsNullOrEmpty(rawUrl))
            {
                foreach (string item in lstStringToCheck)
                {
                    if (rawUrl.StartsWith(item))
                    {
                        res = true;
                        break;
                    }
                }
            }
            return res;
        }

        #endregion

    }
}
