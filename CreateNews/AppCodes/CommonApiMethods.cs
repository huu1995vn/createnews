﻿using Core;
using Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static CreateNews.Variables;

namespace Commons
{
    public static class CommonApiMethods
    {

        public static async Task<CustomResultType<JObject>> Get(string pUrl, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {
            HttpResponseMessage httpRes  =  await  CoreHttp.request_Get(pUrl, pHeaders, mediaType);
            return await CommonApiMethods.ResponseMessage(httpRes);
        }

        public static async Task<CustomResultType<JObject>> Put(string pUrl, object pJsonContent, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {

            HttpResponseMessage httpRes = await CoreHttp.request_Put(pUrl, pJsonContent, pHeaders, mediaType);
            return await CommonApiMethods.ResponseMessage(httpRes);

        }

        public static async Task<CustomResultType<JObject>> Post(string pUrl, object pJsonContent, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {
            HttpResponseMessage httpRes = await CoreHttp.request_Post(pUrl, pJsonContent, pHeaders, mediaType);
            return await CommonApiMethods.ResponseMessage(httpRes);

        }

        public static async Task<CustomResultType<JObject>> Delete(string pUrl, Dictionary<string, string> pHeaders = null, string mediaType = ContentTypeKeys.Json)
        {

            HttpResponseMessage httpRes = await CoreHttp.request_Delete(pUrl, pHeaders, mediaType);
            return await CommonApiMethods.ResponseMessage(httpRes);

        }

        private static async Task<CustomResultType<JObject>> ResponseMessage(HttpResponseMessage httpRes)
        {
            var strRes = string.Empty;
            CustomResultType<JObject> cusRes = new CustomResultType<JObject>();
            if (httpRes.IsSuccessStatusCode)
            {
                using (HttpContent content = httpRes.Content)
                {
                    strRes = await content.ReadAsStringAsync();
                }
            }

            if (!string.IsNullOrEmpty(strRes))
            {
                var resObj = JObject.Parse(strRes);
                cusRes = new CustomResultType<JObject>(resObj);
            }
            return cusRes;
        }
    }

}
