﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateNews
{
    public static class Variables
    {
        public static JObject settings = new JObject();
        public static JObject data_master = new JObject();
        public static string domainApiToken = "http://192.168.11.3";
        public static string domainApi = "http://192.168.11.3";
        public static string domainApiCdn = "http://192.168.11.3";
        public static int folderId = 1394;
        public static string token = string.Empty;
        //public static string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6Ik5ndXnhu4VuIFRy4buNbmcgSOG7r3UiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJkbSIsIklkIjoiMTYiLCJuYmYiOjE1NjkyMjk3NzcsImV4cCI6MTU3MTgyMTc3NywiaXNzIjoiQ8O0bmcgdHkgVE5ISCBnaWFuIGjDoG5nIHRy4buxYyB0dXnhur9uIFZOIiwiYXVkIjoiZGFpbHl4ZS5jb20udm4ifQ.VzeBJQU1z6ESrrt0XEezABvxCwEIggQGwE2y2xL5Zmg";
        public static string userNameAdmin = "tronghuu";
        public static string passAdmin = "40AC3FAC18223DAF66403777BB902108";
        #region "PathApi DaiLyXe"
        public static string pathApiToken = string.Empty;
        public static string pathApiFiles = string.Empty;
        public static string pathApiTinTuc = string.Empty;
        public static string pathES = string.Empty;
        public static string pathApiCommonfile = string.Empty;
        public static string pathApiCommonfile_CreateFileChiTietTinTuc = string.Empty;
        public static void refresh()
        {
            Variables.pathApiToken = string.Format("{0}/api/{1}", Variables.domainApiToken, "token");
            Variables.pathApiFiles = string.Format("{0}/api/dailyxe/{1}", Variables.domainApi, "Files");
            Variables.pathES = string.Format("{0}/api/{1}", Variables.domainApi, "elasticsearch/_post");
            Variables.pathApiTinTuc = string.Format("{0}/api/dailyxe/{1}", Variables.domainApi, "TinTuc");
            Variables.pathApiCommonfile = string.Format("{0}/api/dailyxe/{1}", Variables.domainApi, "Commonfile");
            Variables.pathApiCommonfile_CreateFileChiTietTinTuc = string.Format("{0}/{1}", Variables.pathApiCommonfile, "CreateFileChiTietTinTuc");
        }
        #endregion

        #region "Messages"

        public const string MessageSessionTimeOut = "Phiên làm việc đã hết, bạn vui lòng đăng nhập lại để tiếp tục";
        public const string MessageSessionInvalidRole = "Không có quyền truy cập vào trang này. Bạn vui lòng đăng nhập lại";

        #endregion
        internal static class ColumnKeys
        {
            internal const string Id = "Id";
            internal const string Method = "Method";
            internal const string Url = "Url";
            internal const string Body = "Body";
            internal const string Headers = "Headers";
            internal const string Interval = "Interval";
            internal const string StartDate = "StartDate";
            internal const string Status = "Status";
            internal const string MediaType = "MediaType";


        }

        internal static class ContentTypeKeys
        {
            internal const string FormData = "multipart/form-data";
            internal const string Json = "application/json";
            internal const string FormUrlencoded = "application/x-www-form-urlencoded"; // mặc định cho phương thức post
            internal const string Text = "text/plain; charset=utf-8";
        }
    }
}
