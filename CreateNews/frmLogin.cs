﻿using Commons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CreateNews
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private async void BtnLogin_Click(object sender, EventArgs e)
        {
            if(txtUserName.Text.NullIsEmpty() == string.Empty || txtPass.Text.NullIsEmpty() == string.Empty)
            {
                MessageBox.Show("Tài khoản và mật khẩu không hợp lệ");
                return;
            }
            var userName = txtUserName.Text;
            var passWord = CommonMethods.GetEncryptMD5(txtPass.Text);

            var strToken = await CommonMethods.GetToken(userName, passWord);
            if (strToken.NullIsEmpty() == string.Empty)
            {
                MessageBox.Show("Tài khoản và mật khẩu không hợp lệ");
                return;
            }
            else
            {
                this.Hide();
                frmMain fm = new frmMain();
                fm.Show();
            }
        }
    }
}
