﻿namespace CreateNews
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnLoadHtml = new System.Windows.Forms.Button();
            this.grbTinTuc = new System.Windows.Forms.GroupBox();
            this.txtUrlHinhDaiDien = new System.Windows.Forms.TextBox();
            this.cbbMenus = new System.Windows.Forms.ComboBox();
            this.btnTao = new System.Windows.Forms.Button();
            this.dtpNgayUp = new System.Windows.Forms.DateTimePicker();
            this.ckbTinNoiBat = new System.Windows.Forms.CheckBox();
            this.ptbHinhDaiDien = new System.Windows.Forms.PictureBox();
            this.txtMoTa = new System.Windows.Forms.RichTextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.cbbGetNews = new System.Windows.Forms.ComboBox();
            this.txtContent = new Zoople.HTMLEditControl();
            this.grbTinTuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbHinhDaiDien)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Maroon;
            this.lblTitle.Location = new System.Drawing.Point(271, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(341, 55);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "TẠO TIN TỨC";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(262, 78);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(664, 20);
            this.txtPath.TabIndex = 1;
            // 
            // btnLoadHtml
            // 
            this.btnLoadHtml.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnLoadHtml.Location = new System.Drawing.Point(932, 78);
            this.btnLoadHtml.Name = "btnLoadHtml";
            this.btnLoadHtml.Size = new System.Drawing.Size(75, 23);
            this.btnLoadHtml.TabIndex = 2;
            this.btnLoadHtml.Text = "Load";
            this.btnLoadHtml.UseVisualStyleBackColor = true;
            this.btnLoadHtml.Click += new System.EventHandler(this.BtnLoadHtml_Click);
            // 
            // grbTinTuc
            // 
            this.grbTinTuc.Controls.Add(this.txtContent);
            this.grbTinTuc.Controls.Add(this.txtUrlHinhDaiDien);
            this.grbTinTuc.Controls.Add(this.cbbMenus);
            this.grbTinTuc.Controls.Add(this.btnTao);
            this.grbTinTuc.Controls.Add(this.dtpNgayUp);
            this.grbTinTuc.Controls.Add(this.ckbTinNoiBat);
            this.grbTinTuc.Controls.Add(this.ptbHinhDaiDien);
            this.grbTinTuc.Controls.Add(this.txtMoTa);
            this.grbTinTuc.Controls.Add(this.txtTitle);
            this.grbTinTuc.Location = new System.Drawing.Point(55, 130);
            this.grbTinTuc.Name = "grbTinTuc";
            this.grbTinTuc.Size = new System.Drawing.Size(968, 509);
            this.grbTinTuc.TabIndex = 3;
            this.grbTinTuc.TabStop = false;
            this.grbTinTuc.Text = "Thông tin";
            // 
            // txtUrlHinhDaiDien
            // 
            this.txtUrlHinhDaiDien.Location = new System.Drawing.Point(30, 245);
            this.txtUrlHinhDaiDien.Name = "txtUrlHinhDaiDien";
            this.txtUrlHinhDaiDien.Size = new System.Drawing.Size(156, 20);
            this.txtUrlHinhDaiDien.TabIndex = 7;
            this.txtUrlHinhDaiDien.TextChanged += new System.EventHandler(this.TxtUrlHinhDaiDien_TextChanged);
            // 
            // cbbMenus
            // 
            this.cbbMenus.FormattingEnabled = true;
            this.cbbMenus.Location = new System.Drawing.Point(30, 32);
            this.cbbMenus.Name = "cbbMenus";
            this.cbbMenus.Size = new System.Drawing.Size(156, 21);
            this.cbbMenus.TabIndex = 5;
            // 
            // btnTao
            // 
            this.btnTao.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnTao.Location = new System.Drawing.Point(30, 271);
            this.btnTao.Name = "btnTao";
            this.btnTao.Size = new System.Drawing.Size(156, 23);
            this.btnTao.TabIndex = 4;
            this.btnTao.Text = "Tạo";
            this.btnTao.UseVisualStyleBackColor = true;
            this.btnTao.Click += new System.EventHandler(this.BtnTao_Click);
            // 
            // dtpNgayUp
            // 
            this.dtpNgayUp.Location = new System.Drawing.Point(30, 82);
            this.dtpNgayUp.Name = "dtpNgayUp";
            this.dtpNgayUp.Size = new System.Drawing.Size(156, 20);
            this.dtpNgayUp.TabIndex = 6;
            // 
            // ckbTinNoiBat
            // 
            this.ckbTinNoiBat.AutoSize = true;
            this.ckbTinNoiBat.Location = new System.Drawing.Point(30, 59);
            this.ckbTinNoiBat.Name = "ckbTinNoiBat";
            this.ckbTinNoiBat.Size = new System.Drawing.Size(76, 17);
            this.ckbTinNoiBat.TabIndex = 5;
            this.ckbTinNoiBat.Text = "Tin nổi bật";
            this.ckbTinNoiBat.UseVisualStyleBackColor = true;
            // 
            // ptbHinhDaiDien
            // 
            this.ptbHinhDaiDien.Location = new System.Drawing.Point(30, 109);
            this.ptbHinhDaiDien.Name = "ptbHinhDaiDien";
            this.ptbHinhDaiDien.Size = new System.Drawing.Size(156, 130);
            this.ptbHinhDaiDien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbHinhDaiDien.TabIndex = 3;
            this.ptbHinhDaiDien.TabStop = false;
            // 
            // txtMoTa
            // 
            this.txtMoTa.Location = new System.Drawing.Point(238, 58);
            this.txtMoTa.Name = "txtMoTa";
            this.txtMoTa.Size = new System.Drawing.Size(714, 44);
            this.txtMoTa.TabIndex = 1;
            this.txtMoTa.Text = "";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(238, 32);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(714, 20);
            this.txtTitle.TabIndex = 0;
            // 
            // cbbGetNews
            // 
            this.cbbGetNews.FormattingEnabled = true;
            this.cbbGetNews.Location = new System.Drawing.Point(100, 77);
            this.cbbGetNews.Name = "cbbGetNews";
            this.cbbGetNews.Size = new System.Drawing.Size(156, 21);
            this.cbbGetNews.TabIndex = 4;
            // 
            // txtContent
            // 
            this.txtContent.AcceptsReturn = true;
            this.txtContent.AllowDragInternal = true;
            this.txtContent.BaseURL = null;
            this.txtContent.CleanMSWordHTMLOnPaste = true;
            this.txtContent.CodeEditor.Enabled = true;
            this.txtContent.CodeEditor.Locked = false;
            this.txtContent.CodeEditor.WordWrap = false;
            this.txtContent.CSSText = null;
            this.txtContent.DocumentHTML = null;
            this.txtContent.EditingDisabled = false;
            this.txtContent.EnableInlineSpelling = false;
            this.txtContent.FontsList = null;
            this.txtContent.HiddenButtons = null;
            this.txtContent.ImageStorageLocation = null;
            this.txtContent.InCodeView = false;
            this.txtContent.LanguageFile = null;
            this.txtContent.LicenceActivationKey = null;
            this.txtContent.LicenceKey = null;
            this.txtContent.LicenceKeyInlineSpelling = null;
            this.txtContent.Location = new System.Drawing.Point(238, 109);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(714, 381);
            this.txtContent.SpellingAutoCorrectionList = ((System.Collections.Hashtable)(resources.GetObject("txtContent.SpellingAutoCorrectionList")));
            this.txtContent.TabIndex = 8;
            this.txtContent.ToolstripImageScalingSize = new System.Drawing.Size(16, 16);
            this.txtContent.UseParagraphAsDefault = true;
            this.txtContent.Load += new System.EventHandler(this.HtmlEditControl1_Load);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 651);
            this.Controls.Add(this.grbTinTuc);
            this.Controls.Add(this.cbbGetNews);
            this.Controls.Add(this.btnLoadHtml);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.lblTitle);
            this.Name = "frmMain";
            this.Text = "Tool tạo tin";
            this.Load += new System.EventHandler(this.Main_Load);
            this.grbTinTuc.ResumeLayout(false);
            this.grbTinTuc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbHinhDaiDien)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnLoadHtml;
        private System.Windows.Forms.GroupBox grbTinTuc;
        private System.Windows.Forms.RichTextBox txtMoTa;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Button btnTao;
        private System.Windows.Forms.PictureBox ptbHinhDaiDien;
        private System.Windows.Forms.ComboBox cbbGetNews;
        private System.Windows.Forms.DateTimePicker dtpNgayUp;
        private System.Windows.Forms.CheckBox ckbTinNoiBat;
        private System.Windows.Forms.ComboBox cbbMenus;
        private System.Windows.Forms.TextBox txtUrlHinhDaiDien;
        private Zoople.HTMLEditControl txtContent;
    }
}

